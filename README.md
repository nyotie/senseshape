### What is this repository for? ###

* Quick summary  
An implementation of Shapelet Mining on Inertial data. It basically take input from a csv file, inside must have at least 2 column for time-series and 1 column for label. One csv is one activity / one label. This app work in sequence with 2 other apps : Android app to collect data, and SenseShapeTester to test the Decision Tree Model + Shapelet.  
Android apps to collect data 						: [Koala Simple Apps](https://bitbucket.org/nyotie/koalasimpleapps)  
SenseShape to build DT Model + Extract the Shapelet	: [SenseShape](https://bitbucket.org/nyotie/senseshape)  
SenseShapeTester to test the DT Model and Shapelet	: [SenseShapeTester](https://bitbucket.org/nyotie/senseshapetester)  

* Version: -

### How do I get set up? ###

* Summary of set up  
You run the class from **calculateDistanceMatrix** class. Inside the class, there are some parameter to be changed with **Config :** prefix in comment. List of parameters can be changed : Segmentation, Similarity Calculation, Smoothing Filter, Top-K Ranking, Feature Matrix. The data paths is written in the path. Feel free to change. But make sure to create a new folder with name : **data**  

* Configuration

* Dependencies:
	1. [weka.jar](http://www.cs.waikato.ac.nz/ml/weka/downloading.html)
	2. [opencsv.jar](http://opencsv.sourceforge.net/dependencies.html)
	3. Java version : 1.8

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Anything, feel free to mail  
Author	: Nyoto / garmseven@gmail.com  
MVP		: Daniel (吳政瑋) / silvemoonfox@hotmail.com

* Other community or team contact