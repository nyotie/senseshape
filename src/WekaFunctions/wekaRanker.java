package WekaFunctions;

import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

import java.io.File;

/**
 * Created by nyotie on 30-May-17.
 */
public class wekaRanker {

    // --------------------------------------------------------------------------- FUNCTIONS

    public static String[] applyGainRatio(File rawSource, int getTopN) throws Exception {
        CSVLoader loader = new CSVLoader();
        loader.setSource(rawSource);

        Instances data = loader.getDataSet();
        data.setClassIndex(data.numAttributes() - 1);

        AttributeSelection attribSelect = new AttributeSelection();  // package weka.attributeSelection!
        GainRatioAttributeEval eval = new GainRatioAttributeEval();
        Ranker search = new Ranker();
        search.setThreshold(-1.7976931348623157E308); // default
        search.setNumToSelect(getTopN);
        search.setGenerateRanking(true);
        attribSelect.setEvaluator(eval);
        attribSelect.setSearch(search);

        try {
            attribSelect.SelectAttributes(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int[] indices  = attribSelect.selectedAttributes();
        String[] returnValue = new String[getTopN+1]; // +1 for class label.

        for(int idx = 0; idx<=indices.length-1; idx++){ // the last ranked is always the class, that's why return value has size+1.
            returnValue[idx] = data.attribute(indices[idx]).toString().split(" ")[1];
        }

        return returnValue;
    }

    public static String[] applyInfoGain(File rawSource, int getTopN) throws Exception {
        CSVLoader loader = new CSVLoader();
        loader.setSource(rawSource);

        Instances data = loader.getDataSet();
        data.setClassIndex(data.numAttributes() - 1);

        AttributeSelection attribSelect = new AttributeSelection();  // package weka.attributeSelection!
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        Ranker search = new Ranker();
        search.setThreshold(-1.7976931348623157E308); // default
        search.setNumToSelect(getTopN);
        search.setGenerateRanking(true);
        attribSelect.setEvaluator(eval);
        attribSelect.setSearch(search);

        try {
            attribSelect.SelectAttributes(data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int[] indices  = attribSelect.selectedAttributes();
        String[] returnValue = new String[getTopN+1]; // +1 for class label.

        for(int idx = 0; idx<=indices.length-1; idx++){ // the last ranked is always the class, that's why return value has size+1.
            returnValue[idx] = data.attribute(indices[idx]).toString().split(" ")[1];
        }

        return returnValue;
    }

    // --------------------------------------------------------------------------- EXAMPLE

    public static void main(String[] args) throws Exception {

        File rawSource = new File("D:/experiment/method4/baseline 2/testing 2/Median/dmTransposed/distance_100.csv");

        CSVLoader loader = new CSVLoader();
        loader.setSource(rawSource);
        Instances data = loader.getDataSet();
        data.setClassIndex(data.numAttributes() - 1);

        AttributeSelection attsel = new AttributeSelection();  // package weka.attributeSelection!
        InfoGainAttributeEval eval = new InfoGainAttributeEval();
        Ranker search = new Ranker();
        search.setThreshold(-1.7976931348623157E308);
        search.setNumToSelect(30);
        search.setGenerateRanking(true);
        attsel.setEvaluator(eval);
        attsel.setSearch(search);

        try {
            attsel.SelectAttributes(data);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        // obtain the attribute indices that were selected
        int[] indices  = attsel.selectedAttributes();
//        double[][] rankedAttribuesArray = attsel.rankedAttributes();

        //sort based on usefulness
//        Arrays.sort(rankedAttribuesArray, (first, second) -> -1 * ((Double) first[1]).compareTo(second[1]));

//        System.out.println(attsel.rankedAttributes().length);
//        System.out.println(Arrays.deepToString(attsel.rankedAttributes()));
        for(int idx: indices){
            System.out.println(data.attribute(idx).toString().split(" ")[1]);
        }
    }
}
