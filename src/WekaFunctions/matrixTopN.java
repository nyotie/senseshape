package WekaFunctions;

import Utils.FileWorker;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by nyotie on 28-May-17.
 */
public class matrixTopN {

    // --------------------------------------------------------------------------- FUNCTIONS

    public static int[] GetTopIndex(ArrayList<String> listOfTopN, File rawSource) throws IOException {
        ArrayList<String> tempTopN = new ArrayList<>(listOfTopN); // create duplicate variable, so it can be deleted and speed up the searching process
        Collections.sort(tempTopN);

        CSVReader reader = new CSVReader(new FileReader(rawSource)); // read template csv and store to variable
        String[] nextLine = reader.readNext(); // read only the header
        reader.close();

        // --------------------------- extract top-n from supplied list
        int[] topNIndex = new int[tempTopN.size()];
        int pointer = 0;

        for (int i = 0; i<= nextLine.length -1; i++){
            if(contain(tempTopN, nextLine[i])){
                topNIndex[pointer]= i;
                pointer++;
            }
            if(tempTopN.size() == 0) break; // found all top-n attribute, break the loop
        }

        return topNIndex;
    }

    public static void KeepListedOnly(int[] keepIndex, File originalFile, boolean keepLastIndex) throws IOException {

        // --------------------------------------- prune Distance files
        CSVReader readDistance = new CSVReader(new FileReader(originalFile)); // read template csv and store to variable
        String[] nextDistanceLine;

        String writeDistancePath = originalFile.getParent() + "/temp.csv";
        CSVWriter writeDistance = new CSVWriter(new FileWriter(writeDistancePath, false), ',', CSVWriter.NO_QUOTE_CHARACTER);

        while ((nextDistanceLine = readDistance.readNext()) != null) {
            if(nextDistanceLine.length <= 5) break; // samtingwong!

            StringBuilder writeThis = new StringBuilder("");
            for(int idx: keepIndex){
                if(keepLastIndex) writeThis.append(nextDistanceLine[idx] + ",");
                else if(idx != keepIndex[keepIndex.length-1]) writeThis.append(nextDistanceLine[idx] + ",");
            }
            writeDistance.writeNext(writeThis.toString().split(","));
        }
        writeDistance.close();
        readDistance.close();

        File newFile = new File(writeDistancePath);
        FileWorker.RenameFile(originalFile, newFile);
    }

    public static void KeepListedOnly(int[] keepIndex, File distanceFile, File idFile) throws IOException {

        // --------------------------------------- prune Distance files
        CSVReader readDistance = new CSVReader(new FileReader(distanceFile)); // read template csv and store to variable
        String[] nextDistanceLine;

        String writeDistancePath = distanceFile.getParent() + "/temp.csv";
        CSVWriter writeDistance = new CSVWriter(new FileWriter(writeDistancePath, false), ',', CSVWriter.NO_QUOTE_CHARACTER);

        while ((nextDistanceLine = readDistance.readNext()) != null) {

            String writeThis="";
            for(int idx: keepIndex){
                writeThis+=nextDistanceLine[idx] + ",";
            }
            writeDistance.writeNext(writeThis.split(","));
        }
        writeDistance.close();
        readDistance.close();

        File newFile = new File(writeDistancePath);
        FileWorker.RenameFile(distanceFile, newFile);

        // --------------------------------------- prune IdToRepo files
        CSVReader readId = new CSVReader(new FileReader(idFile));
        String[] nextIdLine;

        String writeIdPath = distanceFile.getParent() + "/temp.csv";
        CSVWriter writeId = new CSVWriter(new FileWriter(writeIdPath, false), ',', CSVWriter.NO_QUOTE_CHARACTER);

        while ((nextIdLine = readId.readNext()) != null) {

            String writeThis="";
            for(int idx: keepIndex){
                writeThis+=nextIdLine[idx] + ",";
            }
            writeId.writeNext(writeThis.split(","));
        }
        writeId.close();
        readId.close();
        newFile = new File(writeIdPath);
        FileWorker.RenameFile(idFile, newFile);
    }

    private static boolean contain(ArrayList<String> collection, String now){
        for (int shapeNum = 0; shapeNum<=collection.size()-1; shapeNum++) {
            if (collection.get(shapeNum).equals(now)) {
                collection.remove(shapeNum);
                return true;
            }
        }
        return false;
    }

    // --------------------------------------------------------------------------- EXAMPLE

    public static void main(String[] args) throws IOException {
        // prep csv file to write the output
        String writePath = "D:/top300DistanceMatrix.csv";
        CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, new File(writePath).exists()), ',', CSVWriter.NO_QUOTE_CHARACTER);

        // read list of top-300 shapelet candidate, stored in csv
        File topNFilePath = new File("D:/experiment/method4/baseline 2/testing 3/gain ratio/top300shapeletName.csv");
        CSVReader readTopN = new CSVReader(new FileReader(topNFilePath)); // read template csv and store to variable
        ArrayList<String> topN = new ArrayList<>(Arrays.asList(readTopN.readNext()));
        Collections.sort(topN);

        // read distance matrix to be pruned by top-n
        File rawSource = new File("D:/experiment/method4/baseline 2/testing 2/Median/dmTransposed/distance_100.csv");
        CSVReader reader = new CSVReader(new FileReader(rawSource)); // read template csv and store to variable
        String[] nextLine;


        // --------------------------- extract top-n from supplied list
        int[] topNIndex = new int[topN.size()+1];
        int pointer = 0;
        boolean firstRow = true;
        while ((nextLine = reader.readNext()) != null) {
            if(firstRow){
                ArrayList<String> tempTopN = new ArrayList<>(topN); // create duplicate variable, so it can be deleted and speed up the searching process
                for (int i = 0; i<= nextLine.length -1; i++){
                    if(contain(tempTopN, nextLine[i])) {
                        topNIndex[pointer]= i;
                        pointer++;
                    }
                }

                firstRow = false;
            }

            String writeThis="";
            for(int idx: topNIndex){
                writeThis+=nextLine[idx] + ",";
            }
            writeThis += nextLine[nextLine.length-1]; // for classLabel
            csvWriter.writeNext(writeThis.split(","));
        }

        csvWriter.close();
        reader.close();

/*

        // --------------------------- extract difference of features from shaplet-candidate and segment

        ArrayList<String> listOfFiles = new ArrayList<>();
        ArrayList<float[]> topKValues = new ArrayList<>(); // to store segment according to top-k id
        ArrayList<String> tempTopK = new ArrayList<>(topK); // create duplicate variable, so it can be deleted and speed up the searching process

        try(Stream<Path> paths = Files.walk(Paths.get(calculateDistanceMatrix.filteredDataPath)).filter(Files::isRegularFile)) { // get the path from raw if not filtered first
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // extract values as listed shapelets in top-k list
        int fileCount = 0;
        for (String thisFilePath: listOfFiles){
            File theCsvFile = new File(thisFilePath);

            String[] shapeletID = tempTopK.get(0).split("_");

            if(shapeletID[0].equals(fileCount)) {
                CSVReader csvFromVictim = new CSVReader(new FileReader(theCsvFile)); // read template csv and store to variable
                String[] headerName = csvFromVictim.readNext();
                int colCount = headerName.length - 1;
                List<String[]> victimValues = csvFromVictim.readAll();
                ArrayList<float[]> oneCSVdata = calculateDistanceMatrix.transpose_toFloat(victimValues, colCount);

                // if next shape is also from this file, continue extract and add to collection
                boolean continueNextSegment = false;
                while (!continueNextSegment){
                    ArrayList<float[]> segmentsFromThisFile = timeSeriesSegmentation.slideWindow(Integer.valueOf(shapeletID[2]), Integer.valueOf(shapeletID[3]), oneCSVdata.get(Integer.valueOf(shapeletID[1])));
                    topKValues.add(segmentsFromThisFile.get(Integer.valueOf(shapeletID[4].substring(1)))); // extract unique feature number from top-K list

                    tempTopK.remove(0); // remove the first index, file is found and extracted
                    shapeletID = tempTopK.get(0).split("_");

                    if (!shapeletID[0].equals(fileCount)){
                        continueNextSegment=true;
                    }
                }
            }

            fileCount++;
        }
*/


    }
}
