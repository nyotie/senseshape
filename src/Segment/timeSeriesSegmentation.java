package Segment;

import java.util.ArrayList;
import java.util.Arrays;

public class timeSeriesSegmentation {
    static int winSize;
    static int slidePoint;
    static ArrayList<float[]> dataSet;
    static ArrayList<float[]> segment;

    public timeSeriesSegmentation(){
        super();
    }

    public timeSeriesSegmentation(int winSize, int slidePoint){
        this.winSize = winSize;
        this.slidePoint = slidePoint;
    }

    public timeSeriesSegmentation(int winSize, int slidePoint, ArrayList<float[]> dataSet){
        this.winSize = winSize;
        this.slidePoint = slidePoint;
        this.dataSet = dataSet;
    }

    public static ArrayList<float[]> slideWindow(){
        segment = slideWindow(winSize, slidePoint, dataSet);

        return segment;
    }

    public static ArrayList<float[]> slideWindow(int winSize, int slidePoint, ArrayList<float[]> dataSet){
        ArrayList<float[]> segment = new ArrayList<>();

        for(float[] partSet: dataSet){ // for each of the column
            for(int i = 0; i <= partSet.length -1; ++i){ // for each value of the part set
                boolean canSegment;

                if(i != 0 && i <= winSize -1) canSegment = i % (winSize-1) == 0;
                else if(i <= winSize-1) canSegment = false;
                else canSegment = (i - (winSize-1)) % slidePoint == 0;

                if(canSegment) segment.add(Arrays.copyOfRange(partSet, (i-winSize)+1,  i+1));
            }
        }

        return segment;
    }

    public static ArrayList<float[]> slideWindow(int winSize, int slidePoint, float[] dataSet){
        ArrayList<float[]> segment = new ArrayList<>();

        for(int i = 0; i <= dataSet.length -1; ++i){
            boolean canSegment;

            if(i != 0 && i <= winSize -1) canSegment = i % (winSize-1) == 0;
            else if(i <= winSize-1) canSegment = false;
            else canSegment = (i - (winSize-1)) % slidePoint == 0;


            if(canSegment) segment.add(Arrays.copyOfRange(dataSet, (i-winSize)+1,  i+1));
        }

        return segment;
    }
}
