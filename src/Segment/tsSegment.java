package Segment;

import java.util.ArrayList;

/**
 * Created by garms on 09-Mar-17.
 */
public class tsSegment {
    int fileNumber;
    int colNumber;
    ArrayList<float[]> data;
    String label;

    public tsSegment(){
        super();
    }

    public tsSegment(int fileNumber, ArrayList<float[]>  data, String label){
        this.fileNumber = fileNumber;
        this.data = data;
        this.label = label;
    }

    // ---------- setter

    public void setFileNumber(int idx){
        this.fileNumber = idx;
    }

    public void setColumnNumber(int idx){
        this.colNumber = idx;
    }

    public void setData(ArrayList<float[]>  data){
        this.data = data;
    }

    public void setLabel(String label){
        this.label = label;
    }

    // ---------- getter

    public int getFileNumber(){
        return fileNumber;
    }

    public int getColumnNumber(){
        return colNumber;
    }

    public ArrayList<float[]> getData(){
        return data;
    }

    public String getLabel(){
        return label;
    }

    public int getIndex(ArrayList<float[]> data){
        return this.data.indexOf(data);
    }
}
