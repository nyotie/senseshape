package distance;

import java.util.ArrayList;

/**
 * This class implements the Dynamic Time Warping algorithm
 * given two sequences
 * <pre>
 *   X = x1, x2,..., xi,..., xn
 *   Y = y1, y2,..., yj,..., ym
 *  </pre>
 *
 * @author		Cheol-Woo Jung (cjung@gatech.edu)
 * @version	1.0
 */
public class DTW {

    protected float[] seq1;
    protected float[] seq2;
    protected int[][] warpingPath;

    protected int n;
    protected int m;
    protected int K;

    protected double warpingDistance;
    protected double accumulatedDist;

    /**
     * Constructs a Dynamic Time Warping object, Instances must be still set.
     */
    public DTW() {
        super();
    }

    /**
     * Constructor
     *
     * @param sample
     * @param template
     */
    public DTW(float[] sample, float[] template) {
        seq1 = sample;
        seq2 = template;

        n = seq1.length;
        m = seq2.length;
        K = 1;

        warpingPath = new int[n + m][2];	// max(n, m) <= K < n + m
        warpingDistance = 0.0;
        accumulatedDist = 0.0;

        this.compute();
    }

    public void compute() {
        double accumulatedDistance = 0.0;

        double[][] d = new double[n][m];	// local distances
        double[][] D = new double[n][m];	// global distances

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                d[i][j] = distanceBetween(seq1[i], seq2[j]);
            }
        }

        D[0][0] = d[0][0];

        // get squared distance for left side
        for (int i = 1; i < n; i++) {
            D[i][0] = d[i][0] + D[i - 1][0];
        }
//        System.out.println(Arrays.deepToString(D));


        // get squared distance for top side
        for (int j = 1; j < m; j++) {
            D[0][j] = d[0][j] + D[0][j - 1];
        }
//        System.out.println(Arrays.deepToString(D));

        // get squared distance + least distance of surrounding
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                accumulatedDistance = Math.min(Math.min(D[i-1][j], D[i-1][j-1]), D[i][j-1]);
                accumulatedDistance += d[i][j];
                D[i][j] = accumulatedDistance;
            }
        }
        accumulatedDistance = D[n - 1][m - 1];
//        System.out.println(Arrays.deepToString(D));

        int i = n - 1;
        int j = m - 1;
        int minIndex = 1;

        warpingPath[K - 1][0] = i;
        warpingPath[K - 1][1] = j;

        while ((i + j) != 0) {
            if (i == 0) {
                j -= 1;
            } else if (j == 0) {
                i -= 1;
            } else {	// i != 0 && j != 0
                double[] array = { D[i - 1][j], D[i][j - 1], D[i - 1][j - 1] };
                minIndex = this.getIndexOfMinimum(array);

                if (minIndex == 0) {
                    i -= 1;
                } else if (minIndex == 1) {
                    j -= 1;
                } else if (minIndex == 2) {
                    i -= 1;
                    j -= 1;
                }
            } // end else
            K++;
            warpingPath[K - 1][0] = i;
            warpingPath[K - 1][1] = j;
        } // end while
        warpingDistance = accumulatedDistance / K;
        this.accumulatedDist = accumulatedDistance;
        this.reversePath(warpingPath);
    }

    /**
     * Changes the order of the warping path (increasing order)
     *
     * @param path	the warping path in reverse order
     */
    protected void reversePath(int[][] path) {
        int[][] newPath = new int[K][2];
        for (int i = 0; i < K; i++) {
            for (int j = 0; j < 2; j++) {
                newPath[i][j] = path[K - i - 1][j];
            }
        }
        warpingPath = newPath;
    }
    /**
     * Returns the warping distance
     *
     * @return
     */
    public double getWarpingDistance() {
        return warpingDistance;
    }
    /**
     * Returns the warping distance
     *
     * @return
     */
    public double getAccumulatedDistance() {
        return accumulatedDist;
    }

    /**
     * Computes a distance between two points
     *
     * @param p1	the point 1
     * @param p2	the point 2
     * @return		the distance between two points
     */
    protected double distanceBetween(double p1, double p2) {
        return (p1 - p2) * (p1 - p2);
    }

    /**
     * Finds the index of the minimum element from the given array
     *
     * @param array		the array containing numeric values
     * @return				the min value among elements
     */
    protected int getIndexOfMinimum(double[] array) {
        int index = 0;
        double val = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] < val) {
                val = array[i];
                index = i;
            }
        }
        return index;
    }

    /**
     *	Returns a string that displays the warping distance and path
     */
    public String toString() {
        String retVal = "Warping Distance: " + warpingDistance + "\n";
        retVal += "Warping Path: {";
        for (int i = 0; i < K; i++) {
            retVal += "(" + warpingPath[i][0] + ", " +warpingPath[i][1] + ")";
            retVal += (i == K - 1) ? "}" : ", ";

        }
        return retVal;
    }

    /**
     * Tests this class
     *
     * @param args	ignored
     */
    public static void main(String[] args) {
//        float[] n2 = {1.5f, 3.9f, 4.1f, 3.3f};
//        float[] n1 = {2.1f, 2.45f, 3.673f, 4.32f, 2.05f, 1.93f, 5.67f, 6.01f};
//        DTW dtw = new DTW(n1, n2);
//        System.out.println(dtw);

        System.out.println("---acc");

        ArrayList<float[]> n = new ArrayList<>();
        n.add(new float[]{-0.89502f,-0.893311f,-0.811523f,-0.884521f,-0.9021f,-0.89209f,-0.807373f,-0.894287f,-0.895264f,-0.893066f,-0.883545f,-0.879639f,-0.894287f,-0.897705f,-0.87915f});
        n.add(new float[]{-0.9021f,-0.89209f,-0.807373f,-0.894287f,-0.895264f,-0.893066f,-0.883545f,-0.879639f,-0.894287f,-0.897705f,-0.87915f,-0.886475f,-0.891602f,-0.896484f,-0.878906f});
        n.add(new float[]{-0.893066f,-0.883545f,-0.879639f,-0.894287f,-0.897705f,-0.87915f,-0.886475f,-0.891602f,-0.896484f,-0.878906f,-0.885742f,-0.892578f,-0.891846f,-0.882812f,-0.88916f});
        n.add(new float[]{-0.87915f,-0.886475f,-0.891602f,-0.896484f,-0.878906f,-0.885742f,-0.892578f,-0.891846f,-0.882812f,-0.88916f,-0.886719f,-0.898193f,-0.895996f,-0.885742f,-0.890381f});
        n.add(new float[]{-0.885742f,-0.892578f,-0.891846f,-0.882812f,-0.88916f,-0.886719f,-0.898193f,-0.895996f,-0.885742f,-0.890381f,-0.889404f,-0.885254f,-0.875488f,-0.844482f,-0.890381f});
        n.add(new float[]{-0.886719f,-0.898193f,-0.895996f,-0.885742f,-0.890381f,-0.889404f,-0.885254f,-0.875488f,-0.844482f,-0.890381f,-0.878906f,-0.881104f,-0.890625f,-0.897217f,-0.88623f});
        n.add(new float[]{-0.889404f,-0.885254f,-0.875488f,-0.844482f,-0.890381f,-0.878906f,-0.881104f,-0.890625f,-0.897217f,-0.88623f,-0.883545f,-0.894775f,-0.897705f,-0.888916f,-0.878418f});
        n.add(new float[]{-0.878906f,-0.881104f,-0.890625f,-0.897217f,-0.88623f,-0.883545f,-0.894775f,-0.897705f,-0.888916f,-0.878418f,-0.885986f,-0.895996f,-0.886963f,-0.875732f,-0.88916f});
        n.add(new float[]{-0.883545f,-0.894775f,-0.897705f,-0.888916f,-0.878418f,-0.885986f,-0.895996f,-0.886963f,-0.875732f,-0.88916f,-0.891846f,-0.883057f,-0.899414f,-0.891602f,-0.896973f});
        n.add(new float[]{-0.885986f,-0.895996f,-0.886963f,-0.875732f,-0.88916f,-0.891846f,-0.883057f,-0.899414f,-0.891602f,-0.896973f,-0.892578f,-0.890137f,-0.893311f,-0.887451f,-0.889893f});
        n.add(new float[]{-0.891846f,-0.883057f,-0.899414f,-0.891602f,-0.896973f,-0.892578f,-0.890137f,-0.893311f,-0.887451f,-0.889893f,-0.881104f,-0.80127f,-0.804932f,-0.843262f,-0.86499f});
        n.add(new float[]{-0.892578f,-0.890137f,-0.893311f,-0.887451f,-0.889893f,-0.881104f,-0.80127f,-0.804932f,-0.843262f,-0.86499f,-0.851318f,-0.875977f,-0.722656f,-0.870605f,-0.989258f});
        n.add(new float[]{-0.881104f,-0.80127f,-0.804932f,-0.843262f,-0.86499f,-0.851318f,-0.875977f,-0.722656f,-0.870605f,-0.989258f,-1.03467f,-1.02344f,-1.03296f,-0.741211f,-0.916748f});
        n.add(new float[]{-0.851318f,-0.875977f,-0.722656f,-0.870605f,-0.989258f,-1.03467f,-1.02344f,-1.03296f,-0.741211f,-0.916748f,-0.946289f,-0.952881f,-1.01465f,-0.794434f,-0.804443f});
        n.add(new float[]{-1.03467f,-1.02344f,-1.03296f,-0.741211f,-0.916748f,-0.946289f,-0.952881f,-1.01465f,-0.794434f,-0.804443f,-0.852539f,-0.882324f,-0.796875f,-0.772949f,-0.682617f});

        for (int i = 1; i <= n.size()-1; i++){
            DTW thisDTW = new DTW(n.get(i-1), n.get(i));
            System.out.print("distance n"+ (i-1) +" to n"+ i +". accumulated=" + thisDTW.getAccumulatedDistance());
            System.out.println(". warping=" + thisDTW.getWarpingDistance());
        }

        System.out.println("---gyro");

        n.clear();
        n.add(new float[]{-9.91821f,-10.2539f,-10.498f,-10.2539f,-10.2234f,-10.0708f,-10.2844f,-10.2539f,-9.82666f,-10.4065f,-10.5896f,-10.2539f,-9.61304f,-10.1318f,-9.82666f});
        n.add(new float[]{-10.2234f,-10.0708f,-10.2844f,-10.2539f,-9.82666f,-10.4065f,-10.5896f,-10.2539f,-9.61304f,-10.1318f,-9.82666f,-10.3455f,-9.64355f,-10.5286f,-10.2234f});
        n.add(new float[]{-10.4065f,-10.5896f,-10.2539f,-9.61304f,-10.1318f,-9.82666f,-10.3455f,-9.64355f,-10.5286f,-10.2234f,-10.376f,-9.67407f,-9.8877f,-10.1013f,-10.1013f});
        n.add(new float[]{-9.82666f,-10.3455f,-9.64355f,-10.5286f,-10.2234f,-10.376f,-9.67407f,-9.8877f,-10.1013f,-10.1013f,-10.3455f,-10.4065f,-10.0708f,-10.0708f,-9.97925f});
        n.add(new float[]{-10.376f,-9.67407f,-9.8877f,-10.1013f,-10.1013f,-10.3455f,-10.4065f,-10.0708f,-10.0708f,-9.97925f,-10.0708f,-10.7117f,-10.8643f,-10.3455f,-10.3455f});
        n.add(new float[]{-10.3455f,-10.4065f,-10.0708f,-10.0708f,-9.97925f,-10.0708f,-10.7117f,-10.8643f,-10.3455f,-10.3455f,-10.7727f,-10.6506f,-10.1929f,-10.7422f,-10.7117f});
        n.add(new float[]{-10.0708f,-10.7117f,-10.8643f,-10.3455f,-10.3455f,-10.7727f,-10.6506f,-10.1929f,-10.7422f,-10.7117f,-10.6506f,-10.4065f,-10.8032f,-11.0474f,-10.7727f});
        n.add(new float[]{-10.7727f,-10.6506f,-10.1929f,-10.7422f,-10.7117f,-10.6506f,-10.4065f,-10.8032f,-11.0474f,-10.7727f,-10.5896f,-10.498f,-10.6201f,-10.7422f,-10.6812f});
        n.add(new float[]{-10.6506f,-10.4065f,-10.8032f,-11.0474f,-10.7727f,-10.5896f,-10.498f,-10.6201f,-10.7422f,-10.6812f,-10.4675f,-11.261f,-11.1694f,-10.7117f,-11.6577f});
        n.add(new float[]{-10.5896f,-10.498f,-10.6201f,-10.7422f,-10.6812f,-10.4675f,-11.261f,-11.1694f,-10.7117f,-11.6577f,-11.1694f,-11.3525f,-11.0474f,-11.2f,-11.2f});
        n.add(new float[]{-10.4675f,-11.261f,-11.1694f,-10.7117f,-11.6577f,-11.1694f,-11.3525f,-11.0474f,-11.2f,-11.2f,-4.21143f,-4.36401f,-4.24194f,-17.4866f,-14.4043f});
        n.add(new float[]{-11.1694f,-11.3525f,-11.0474f,-11.2f,-11.2f,-4.21143f,-4.36401f,-4.24194f,-17.4866f,-14.4043f,-15.564f,-27.2827f,-22.7966f,-31.9519f,-32.2571f});
        n.add(new float[]{-4.21143f,-4.36401f,-4.24194f,-17.4866f,-14.4043f,-15.564f,-27.2827f,-22.7966f,-31.9519f,-32.2571f,-30.1208f,-36.9568f,-48.1873f,-34.5764f,-15.6555f});
        n.add(new float[]{-15.564f,-27.2827f,-22.7966f,-31.9519f,-32.2571f,-30.1208f,-36.9568f,-48.1873f,-34.5764f,-15.6555f,-58.197f,-36.1633f,-74.0967f,-81.5735f,-88.9893f});
        n.add(new float[]{-30.1208f,-36.9568f,-48.1873f,-34.5764f,-15.6555f,-58.197f,-36.1633f,-74.0967f,-81.5735f,-88.9893f,-97.0154f,-93.5364f,-99.2126f,-104.156f,-102.386f});

        for (int i = 1; i <= n.size()-1; i++){
            DTW thisDTW = new DTW(n.get(i-1), n.get(i));
            System.out.print("distance n"+ (i-1) +" to n"+ i +". accumulated=" + thisDTW.getAccumulatedDistance());
            System.out.println(". warping=" + thisDTW.getWarpingDistance());
        }
    }
}
