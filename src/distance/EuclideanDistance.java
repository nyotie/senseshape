package distance;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import java.util.ArrayList;

/**
 * Created by nyotie on 04-May-17.
 */
public class EuclideanDistance {

    private float[] seq1;
    private float[] seq2;

    protected int seq1Length;
    protected int seq2Length;

    private double totalDistance;

    // --------------------------------------------------------------------------- SETTER AND GETTER

    private boolean checkData(){
        try{
            if(seq1 == null) throw new NullPointerException();
            if(seq2 == null) throw new NullPointerException();
            if(seq1.length == 0) throw new IllegalArgumentException();
            if(seq2.length == 0) throw new IllegalArgumentException();

            this.setSeq1(seq1);
            this.setSeq2(seq2);

            seq1Length = seq1.length;
            seq2Length = seq2.length;

            if(seq1Length != seq2Length) throw new Exception("sequence1 and sequence2 have to be in equal length!");
        } catch (NullPointerException err1){
            System.out.println("Can't handle null arrays");
            return false;
        } catch (IllegalArgumentException err2){
            System.out.println("Can't handle zero-length arrays.");
            return false;
        } catch (Exception err3) {
            err3.printStackTrace();
            return false;
        }

        return true;
    }

    private float[] getSeq1() {
        return seq1;
    }

    public void setSeq1(float[] seq1) {
        this.seq1 = seq1;
    }

    private float[] getSeq2() {
        return seq2;
    }

    public void setSeq2(float[] seq2) {
        this.seq2 = seq2;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    // --------------------------------------------------------------------------- CALLER

    /**
     * Constructs an distance.EuclideanDistance object, Instances must be still set.
     */
    public EuclideanDistance() {
        super();
    }

    /**
     * Constructor
     *
     * @param seq1
     * @param seq2
     */
    public EuclideanDistance(float[] seq1, float[] seq2) {
        setSeq1(seq1);
        setSeq2(seq2);

        this.compute();
    }

    // --------------------------------------------------------------------------- FEATURE CALCULATIONS

    private double getVar(float[] seq) {
        Variance var = new Variance();
        for (Float pt : seq) {
            var.increment(pt);
        }
        return var.getResult();
    }

    private double getMean(float[] seq){
        Mean mean = new Mean();
        for (Float pt : seq) {
            mean.increment(pt);
        }
        return mean.getResult();
    }

    private double getStandardDeviation(double variance){
        return Math.sqrt(variance);
    }

    private double normalizedDistanceBetween(double px, double mx, double sx, double py, double my, double sy){
        double x = (px-mx)/sx;
        double y = (py-my)/sy;

//        System.out.println((x - y) * (x - y));
        return (x - y) * (x - y);
    }

    public void compute() {
        checkData();

        // get mean
        double mx = getMean(getSeq1());
        double my = getMean(getSeq2());

//        System.out.println("mx:" + mx);
//        System.out.println("my" + my);

        // get variance
        double vx = getVar(getSeq1());
        double vy = getVar(getSeq2());

//        System.out.println("vx:" + vx);
//        System.out.println("vy:" + vy);

        // get standard deviation
        double sx = getStandardDeviation(vx);
        double sy = getStandardDeviation(vy);

//        System.out.println("sx:" + sx);
//        System.out.println("sy:" + sy);

        setTotalDistance(0.0);
        for(int i = 0; i <= seq1Length - 1; i++){
            this.setTotalDistance(this.getTotalDistance() + normalizedDistanceBetween(getSeq1()[i], mx, sx, getSeq2()[i], my, sy));
        }

        setTotalDistance(Math.sqrt(getTotalDistance()));
    }

    public static void main(String[] args) {
//        float[] n2 = {1.5f, 3.9f, 4.1f, 3.3f};
//        float[] n1 = {2.1f, 2.45f, 3.673f, 4.32f};
//        EuclideanDistance calcED = new EuclideanDistance(n1, n2);
//        System.out.println("distance=" + calcED.getTotalDistance());


        System.out.println("---acc");

        ArrayList<float[]> n = new ArrayList<>();
        n.add(new float[]{-0.89502f,-0.893311f,-0.811523f,-0.884521f,-0.9021f,-0.89209f,-0.807373f,-0.894287f,-0.895264f,-0.893066f,-0.883545f,-0.879639f,-0.894287f,-0.897705f,-0.87915f});
        n.add(new float[]{-0.9021f,-0.89209f,-0.807373f,-0.894287f,-0.895264f,-0.893066f,-0.883545f,-0.879639f,-0.894287f,-0.897705f,-0.87915f,-0.886475f,-0.891602f,-0.896484f,-0.878906f});
        n.add(new float[]{-0.893066f,-0.883545f,-0.879639f,-0.894287f,-0.897705f,-0.87915f,-0.886475f,-0.891602f,-0.896484f,-0.878906f,-0.885742f,-0.892578f,-0.891846f,-0.882812f,-0.88916f});
        n.add(new float[]{-0.87915f,-0.886475f,-0.891602f,-0.896484f,-0.878906f,-0.885742f,-0.892578f,-0.891846f,-0.882812f,-0.88916f,-0.886719f,-0.898193f,-0.895996f,-0.885742f,-0.890381f});
        n.add(new float[]{-0.885742f,-0.892578f,-0.891846f,-0.882812f,-0.88916f,-0.886719f,-0.898193f,-0.895996f,-0.885742f,-0.890381f,-0.889404f,-0.885254f,-0.875488f,-0.844482f,-0.890381f});
        n.add(new float[]{-0.886719f,-0.898193f,-0.895996f,-0.885742f,-0.890381f,-0.889404f,-0.885254f,-0.875488f,-0.844482f,-0.890381f,-0.878906f,-0.881104f,-0.890625f,-0.897217f,-0.88623f});
        n.add(new float[]{-0.889404f,-0.885254f,-0.875488f,-0.844482f,-0.890381f,-0.878906f,-0.881104f,-0.890625f,-0.897217f,-0.88623f,-0.883545f,-0.894775f,-0.897705f,-0.888916f,-0.878418f});
        n.add(new float[]{-0.878906f,-0.881104f,-0.890625f,-0.897217f,-0.88623f,-0.883545f,-0.894775f,-0.897705f,-0.888916f,-0.878418f,-0.885986f,-0.895996f,-0.886963f,-0.875732f,-0.88916f});
        n.add(new float[]{-0.883545f,-0.894775f,-0.897705f,-0.888916f,-0.878418f,-0.885986f,-0.895996f,-0.886963f,-0.875732f,-0.88916f,-0.891846f,-0.883057f,-0.899414f,-0.891602f,-0.896973f});
        n.add(new float[]{-0.885986f,-0.895996f,-0.886963f,-0.875732f,-0.88916f,-0.891846f,-0.883057f,-0.899414f,-0.891602f,-0.896973f,-0.892578f,-0.890137f,-0.893311f,-0.887451f,-0.889893f});
        n.add(new float[]{-0.891846f,-0.883057f,-0.899414f,-0.891602f,-0.896973f,-0.892578f,-0.890137f,-0.893311f,-0.887451f,-0.889893f,-0.881104f,-0.80127f,-0.804932f,-0.843262f,-0.86499f});
        n.add(new float[]{-0.892578f,-0.890137f,-0.893311f,-0.887451f,-0.889893f,-0.881104f,-0.80127f,-0.804932f,-0.843262f,-0.86499f,-0.851318f,-0.875977f,-0.722656f,-0.870605f,-0.989258f});
        n.add(new float[]{-0.881104f,-0.80127f,-0.804932f,-0.843262f,-0.86499f,-0.851318f,-0.875977f,-0.722656f,-0.870605f,-0.989258f,-1.03467f,-1.02344f,-1.03296f,-0.741211f,-0.916748f});
        n.add(new float[]{-0.851318f,-0.875977f,-0.722656f,-0.870605f,-0.989258f,-1.03467f,-1.02344f,-1.03296f,-0.741211f,-0.916748f,-0.946289f,-0.952881f,-1.01465f,-0.794434f,-0.804443f});
        n.add(new float[]{-1.03467f,-1.02344f,-1.03296f,-0.741211f,-0.916748f,-0.946289f,-0.952881f,-1.01465f,-0.794434f,-0.804443f,-0.852539f,-0.882324f,-0.796875f,-0.772949f,-0.682617f});

        for (int i = 1; i <= n.size()-1; i++){
            EuclideanDistance thisEd = new EuclideanDistance(n.get(i-1), n.get(i));
            System.out.println("distance n"+ (i-1) +" to n"+ i +"=" + thisEd.getTotalDistance());
        }

        System.out.println("---gyro");

        n.clear();
        n.add(new float[]{-9.91821f,-10.2539f,-10.498f,-10.2539f,-10.2234f,-10.0708f,-10.2844f,-10.2539f,-9.82666f,-10.4065f,-10.5896f,-10.2539f,-9.61304f,-10.1318f,-9.82666f});
        n.add(new float[]{-10.2234f,-10.0708f,-10.2844f,-10.2539f,-9.82666f,-10.4065f,-10.5896f,-10.2539f,-9.61304f,-10.1318f,-9.82666f,-10.3455f,-9.64355f,-10.5286f,-10.2234f});
        n.add(new float[]{-10.4065f,-10.5896f,-10.2539f,-9.61304f,-10.1318f,-9.82666f,-10.3455f,-9.64355f,-10.5286f,-10.2234f,-10.376f,-9.67407f,-9.8877f,-10.1013f,-10.1013f});
        n.add(new float[]{-9.82666f,-10.3455f,-9.64355f,-10.5286f,-10.2234f,-10.376f,-9.67407f,-9.8877f,-10.1013f,-10.1013f,-10.3455f,-10.4065f,-10.0708f,-10.0708f,-9.97925f});
        n.add(new float[]{-10.376f,-9.67407f,-9.8877f,-10.1013f,-10.1013f,-10.3455f,-10.4065f,-10.0708f,-10.0708f,-9.97925f,-10.0708f,-10.7117f,-10.8643f,-10.3455f,-10.3455f});
        n.add(new float[]{-10.3455f,-10.4065f,-10.0708f,-10.0708f,-9.97925f,-10.0708f,-10.7117f,-10.8643f,-10.3455f,-10.3455f,-10.7727f,-10.6506f,-10.1929f,-10.7422f,-10.7117f});
        n.add(new float[]{-10.0708f,-10.7117f,-10.8643f,-10.3455f,-10.3455f,-10.7727f,-10.6506f,-10.1929f,-10.7422f,-10.7117f,-10.6506f,-10.4065f,-10.8032f,-11.0474f,-10.7727f});
        n.add(new float[]{-10.7727f,-10.6506f,-10.1929f,-10.7422f,-10.7117f,-10.6506f,-10.4065f,-10.8032f,-11.0474f,-10.7727f,-10.5896f,-10.498f,-10.6201f,-10.7422f,-10.6812f});
        n.add(new float[]{-10.6506f,-10.4065f,-10.8032f,-11.0474f,-10.7727f,-10.5896f,-10.498f,-10.6201f,-10.7422f,-10.6812f,-10.4675f,-11.261f,-11.1694f,-10.7117f,-11.6577f});
        n.add(new float[]{-10.5896f,-10.498f,-10.6201f,-10.7422f,-10.6812f,-10.4675f,-11.261f,-11.1694f,-10.7117f,-11.6577f,-11.1694f,-11.3525f,-11.0474f,-11.2f,-11.2f});
        n.add(new float[]{-10.4675f,-11.261f,-11.1694f,-10.7117f,-11.6577f,-11.1694f,-11.3525f,-11.0474f,-11.2f,-11.2f,-4.21143f,-4.36401f,-4.24194f,-17.4866f,-14.4043f});
        n.add(new float[]{-11.1694f,-11.3525f,-11.0474f,-11.2f,-11.2f,-4.21143f,-4.36401f,-4.24194f,-17.4866f,-14.4043f,-15.564f,-27.2827f,-22.7966f,-31.9519f,-32.2571f});
        n.add(new float[]{-4.21143f,-4.36401f,-4.24194f,-17.4866f,-14.4043f,-15.564f,-27.2827f,-22.7966f,-31.9519f,-32.2571f,-30.1208f,-36.9568f,-48.1873f,-34.5764f,-15.6555f});
        n.add(new float[]{-15.564f,-27.2827f,-22.7966f,-31.9519f,-32.2571f,-30.1208f,-36.9568f,-48.1873f,-34.5764f,-15.6555f,-58.197f,-36.1633f,-74.0967f,-81.5735f,-88.9893f});
        n.add(new float[]{-30.1208f,-36.9568f,-48.1873f,-34.5764f,-15.6555f,-58.197f,-36.1633f,-74.0967f,-81.5735f,-88.9893f,-97.0154f,-93.5364f,-99.2126f,-104.156f,-102.386f});

        for (int i = 1; i <= n.size()-1; i++){
            EuclideanDistance thisEd = new EuclideanDistance(n.get(i-1), n.get(i));
            System.out.println("distance n"+ (i-1) +" to n"+ i +"=" + thisEd.getTotalDistance());
        }
    }
}
