package Filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by nyotie on 07-May-17.
 */
public class MeanMedian {

    private int wsize = 10;
    private int FilterType = 0; // 0=Mean, 1=Median
    private ArrayList<Float> samples = new ArrayList<>();
    private float latestValue = 0;

    // --------------------------------------------------------------------------- SETTER AND GETTER

    public int getWsize() {
        return wsize;
    }

    /**
     * Set up window size to store sample. Default is 10
     *
     * @param wsize
     */
    public void setWsize(int wsize, boolean reset) {
        this.wsize = wsize;

        if(reset)
            samples.clear();
        else {
            if (samples.size() > wsize) {
                int countToRemove = samples.size() - wsize;
                for (int i = 0; i <= countToRemove-1; i++) {
                    samples.remove(0);
                }
            }
        }
    }

    public String getFilterType() {
        switch (FilterType){
            case 0:
                return "Mean";
            case 1:
                return "Median";
        }

        return "";
    }

    /**
     * Set the type of the filter. Default is Mean
     *
     * @param filterType 0 for Mean, 1 for Median
     */
    public void setFilterType(int filterType) {
        this.FilterType = filterType;
    }

    public float getLatestValue() {
        return latestValue;
    }

    public ArrayList<Float> getSampleValues() {
        return samples;
    }

    // --------------------------------------------------------------------------- FEATURE CALCULATIONS & CALLER

    public MeanMedian(){
        super();
    }

    public MeanMedian(int wsize, int FilterType){
        this.wsize = wsize;
        this.FilterType = FilterType;
    }

    public float updateSample(float sampleValue){
        if(samples.size() >= wsize)
            samples.remove(0);
        samples.add(sampleValue);

        if(FilterType==0)
            latestValue = getMeanValue(samples);
        else
            latestValue = getMedianValue(samples);

        return latestValue;
    }

    public static float getMedianValue(ArrayList<Float> samples){
        ArrayList<Float> tempSample = new ArrayList<>(samples);
        Collections.sort(tempSample);
//        System.out.println("sorted temp : " + Arrays.toString(tempSample.toArray()));

//        System.out.print("temp size : " + tempSample.size());
        int centerIndex = (tempSample.size()==1) ? 0 : tempSample.size() / 2;
//        System.out.println(". idx : "+ centerIndex);

        if (tempSample.size() % 2 == 0){
            return (tempSample.get(centerIndex-1) + tempSample.get(centerIndex))/2;
        } else {
            return tempSample.get(centerIndex);
        }
    }

    public static float getMeanValue(ArrayList<Float> samples){
        float total = 0;
        for(float num: samples) total+=num;

        return total/samples.size();
    }

    // --------------------------------------------------------------------------- EXAMPLE

    public static void main(String[] args){
        ArrayList<Float> n1 = new ArrayList<>(Arrays.asList(2.1f, 2.45f, 3.673f, 4.32f, 2.05f, 1.93f, 5.67f, 6.01f));
        System.out.println(MeanMedian.getMedianValue(n1));
        System.out.println(MeanMedian.getMeanValue(n1));

        System.out.println("-----------------------------------");

        MeanMedian mmFilter = new MeanMedian();
        mmFilter.setWsize(7, true);
        mmFilter.setFilterType(1);
        System.out.println(mmFilter.getFilterType() + " with winSize of " + mmFilter.getWsize());
        System.out.println();

        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));


        mmFilter.updateSample(2.1f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(2.45f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(3.673f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(4.32f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(2.05f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(1.93f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(5.67f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(6.01f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.setWsize(3, false);
        System.out.println("change window size to : " + mmFilter.getWsize());

        mmFilter.updateSample(4.72f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(5.13f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());

        mmFilter.updateSample(4.26f);
        System.out.println(Arrays.toString(mmFilter.getSampleValues().toArray()));
        System.out.println("latest value: " + mmFilter.getLatestValue());
    }
}
