package Filter;

/**
 * Created by nyotie on 07-May-17.
 */
public class LowPass {

    /*
     * time smoothing constant for low-pass filter
     * 0 ≤ alpha ≤ 1 ; a smaller value basically means more smoothing
     * See: http://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization
     * http://blog.thomnichols.org/2011/08/smoothing-sensor-data-with-a-low-pass-filter
     * https://www.built.io/blog/applying-low-pass-filter-to-android-sensor-s-readings
     */
    static final float ALPHA = 0.45f;
    public static float[] filter( float[] input, float[] output ) {
        if ( output == null ) return input;

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }
        return output;
    }

    public static float[] filter( String[] input, float[] output ) {

        float[] sensorValue = new float[input.length];
        for ( int i=0; i<input.length; i++ ) {
            sensorValue[i] = Float.valueOf(input[i]);
        }

        if ( output == null ) return sensorValue;

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA * (sensorValue[i] - output[i]);
        }
        return output;
    }
}
