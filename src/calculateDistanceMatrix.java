import Features.ExtractStatisticFeatures;
import Features.FeatureExtract;
import Filter.LowPass;
import Filter.MeanMedian;
import Segment.timeSeriesSegmentation;
import Segment.tsSegment;
import Utils.FileWorker;
import WekaFunctions.matrixTopN;
import WekaFunctions.wekaRanker;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import distance.DTW;
import distance.EuclideanDistance;
import distance.PearsonCorrelation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.*;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class calculateDistanceMatrix {
    private static int fileCount=0;
    private static String[] headerName;
    private static int colCount=0;
    private static int minRowCount =0;

    // paths
    private static String rawDataPath = "./data/raw";
    private static String filteredDataPath = "./data/filtered";
    private static String transposedPath = "./data/dmTransposed/";

    // file name
    private static String fnDistanceMatrix = "distance";
    private static String fnFeatureMatrix = "feature";
    private static String fnFeatureNoClass = "FeatNoClass";
    private static String fnIdToRepo = "id";
    private static String fnRepo = "repo";
    private static String fnDistanceFeature = "FeatDist";
    private static String fnCombination = "CombineAll";

    // Config : Segmentation
    private static int startWinSize = 40, endWinSize = 40;
    private static int startSlideSize = 5, endSlideSize = 5;

    // Config : Similarity Calculation Method
    private static int distanceMeasurement = 0; // 0=DTW, 1=ED, 2=PCC

    // Config : Smoothing Filter
    private static boolean applyFilter = true;
    private static int filterMethod = 1; // 0=Mean, 1=Median, 2=LPF
    private static int filterWindowSize = 10;

    // Config : Top-K Ranking
    private static boolean applyRanking = true;
    private static int RankingMethod = 1; // 0=Information Gain, 1=Gain Ratio
    private static int topN = 500;

    // Config : Feature Matrix
    private static boolean applyFeature = true;

    public static void main(String[] args) throws Exception {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                        .withLocale( Locale.UK )
                        .withZone( ZoneId.systemDefault() );
        Instant start = Instant.now();
        System.out.println("Starting mission at: " + formatter.format(Instant.now()));
        System.out.println("Setup:");
        System.out.println("--Segmentation window: " + startWinSize + "~" + endWinSize);
        System.out.println("--Sliding point from: " + startSlideSize);
        if(applyFilter){
            System.out.println("--Filter first");
            System.out.println("--Filter using: " + filterMethod + ", window size:" + filterWindowSize + ". *0=Mean, 1=Median, 2=LPF");
        }
        System.out.println("--Similarity using: " + distanceMeasurement + ". *0=DTW, 1=ED, 2=PCC");
        if (applyRanking){
            System.out.println("--Ranking applied");
            System.out.println("--Rank and prune shapelet using: "+ RankingMethod +" , Top-N:" + topN + ". *0=Information Gain, 1=Gain Ratio");
        }
        System.out.println("--Compute: Distance" + ((applyFilter)? " and Feature " : " only ") + "of shapelet candidate and segment");
        System.out.println();

        // prepare grand parent directory folders
        prepareEnvironment();

        // local variable used across this main
        ArrayList<String> listOfFilesRaw = new ArrayList<>();
        ArrayList<String> listOfDM = new ArrayList<>();
        ArrayList<String> listOfFM = new ArrayList<>();
        ArrayList<String> listOfFDM = new ArrayList<>();
        ArrayList<String> listOfFilesId = new ArrayList<>();
        ArrayList<String> listOfFilesRepo = new ArrayList<>();
        ArrayList<tsSegment> tsCollection = new ArrayList<>();

        /*--------------------------------------------------------------------------------------------------------------
        //Filter / smoothing
        //--------------------------------------------------------------------------------------------------------------*/
        if(applyFilter) {
            System.out.println();
            System.out.println("Filter the raw data to smooth it.");
            filterTimeSeries(listOfFilesRaw);
            System.out.println("DONE Smoothing at " + formatter.format(Instant.now()));
        }

        /*--------------------------------------------------------------------------------------------------------------
        //create dimension matrix
        --------------------------------------------------------------------------------------------------------------*/
        System.out.println();
        System.out.println("Segment and measure the distance. Start:" + formatter.format(Instant.now()));

        // create path bank of template from dir
        if(listOfFilesRaw.size() == 0){
            try(Stream<Path> paths = Files.walk(Paths.get(rawDataPath)).filter(Files::isRegularFile)) { // get the path from raw if not filtered first
                paths.forEach(filePath -> listOfFilesRaw.add(filePath.toString()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ReadAndTransposeToCollection(listOfFilesRaw, tsCollection);

        // extract the label for each file
        String columnForClassLabel = "classLabel,";
        for (int i = 0; i <= fileCount - 1; ++i) {
            columnForClassLabel += tsCollection.get(i).getLabel() + ((i == fileCount - 1) ? "" : ",");
        }

        // segment --> measure the distance --> create matrix
        if(endWinSize > minRowCount) {
            endWinSize = (minRowCount /10) * 10;
            System.out.println("--update max window size to " + endWinSize + " due to max size of data length"); // FIXME: window size is constrained with the length of the segment.
        }

        int numOfCandidateShapelet = 0;
        for(int loopWinSize = startWinSize; loopWinSize <= endWinSize; loopWinSize+=10){
            ArrayList<tsSegment> thisWindowSegments;

            System.out.print("--start segmentation with winSize=" + loopWinSize + ".");
            for(int loopSlidePoint = startSlideSize; loopSlidePoint <= endSlideSize; loopSlidePoint+=5){
                System.out.print(" s=" + loopSlidePoint);
                thisWindowSegments = segmentTimeSeries(tsCollection, loopWinSize, loopSlidePoint);

                for (tsSegment seg: thisWindowSegments){
                    numOfCandidateShapelet += seg.getData().size();
                }

                if(thisWindowSegments.size()>0){
                    calculateDistance(tsCollection, thisWindowSegments, loopWinSize, loopSlidePoint, transposedPath);
                }
            }
            listOfDM.add(transposedPath + fnDistanceMatrix + "_" + loopWinSize + ".csv");
            listOfFilesId.add(transposedPath + fnIdToRepo + "_" + loopWinSize + ".csv");
            listOfFilesRepo.add(transposedPath + fnRepo + "_" + loopWinSize + ".csv");

            System.out.print(". Candidate: " + numOfCandidateShapelet);
            System.out.println(". Finish segmenting at "  + formatter.format(Instant.now()));
            numOfCandidateShapelet=0;
        }
        System.out.println("DONE : Distance calculation at " + formatter.format(Instant.now()));

        /*--------------------------------------------------------------------------------------------------------------
        //Combine into one HUGE file
        --------------------------------------------------------------------------------------------------------------*/

//            System.out.println();
//            System.out.println("Combine into one HUGE file at " + Instant.now());
//            String CombineAllPath = transposedPath + "/CombineAll.csv";
//            FileWorker.MergeFileVertically(listOfFilesDistance, CombineAllPath);
//            listOfFilesDistance.add(CombineAllPath);
//            System.out.println("DONE");

        /*--------------------------------------------------------------------------------------------------------------
        //Transpose to create proper matrix format
        --------------------------------------------------------------------------------------------------------------*/

        System.out.println();
        System.out.println("Fix distance matrix to correct format");
        System.out.print("--Fix distance matrix files:");
        ReadTransposeRenameFiles(listOfDM, columnForClassLabel);
        System.out.println(" finish at:" + formatter.format(Instant.now()));
        if(applyFeature){
            System.out.print("--Fix id matrix files:");
            ReadTransposeRenameFiles(listOfFilesId, columnForClassLabel);
            System.out.println(" finish at:" + formatter.format(Instant.now()));
        }
        System.out.println("DONE Fix-transposing at " + formatter.format(Instant.now()));

//            listOfFilesDistance.add("./data/dmTransposed/distance_40.csv");
//            listOfFilesId.add("./data/dmTransposed/id_40.csv");
//            listOfFilesRepo.add("./data/dmTransposed/repo_40.csv");

        if(applyRanking){
            // FIXME: Show attribute count before ranking top-N is applied.
            /*--------------------------------------------------------------------------------------------------------------
            // Get Top-N segments by ranking it using Information Gain / Gain Ratio.
            --------------------------------------------------------------------------------------------------------------*/
            System.out.println();
            System.out.println("Get Top-N segments by ranking it using Information Gain / Gain Ratio.");
            keepOnlyTopNCandidate(listOfDM, topN);
            System.out.println("DONE Removing at " + formatter.format(Instant.now()));
        }

        if(applyFeature){
            /*--------------------------------------------------------------------------------------------------------------
            // Compute Difference of Feature-value from shapelet candidate and a segment with least distance to candidate.
            --------------------------------------------------------------------------------------------------------------*/

            System.out.println();
            System.out.println("Compute Difference of Feature-value from shapelet candidate and a segment with least distance to candidate.");
            calculateDiffOfFeatureValue(tsCollection, listOfFilesId, listOfFM, columnForClassLabel);
            System.out.println("DONE Computing at " + formatter.format(Instant.now()));

            /*--------------------------------------------------------------------------------------------------------------
            // Combine DistanceMatrix and FeatureMatrix
            --------------------------------------------------------------------------------------------------------------*/

            System.out.println();
            System.out.println("Combine DistanceMatrix and FeatureMatrix.");
            for (String distancePath: listOfDM){
                File distanceFile = new File(distancePath);
                String FeatWoClassPath = distanceFile.getParent() + "/" + fnFeatureNoClass + "_" + distanceFile.getName().split("_")[1];
                String FeatDistMatrixPath = distanceFile.getParent() + "/" + fnDistanceFeature + "_" + distanceFile.getName().split("_")[1];

                FileWorker.MergeFileHorizontally(FeatWoClassPath, distancePath, FeatDistMatrixPath, true, false);
                listOfFDM.add(FeatDistMatrixPath);
            }
            System.out.println("DONE Combining at " + formatter.format(Instant.now()));
        }

        /*--------------------------------------------------------------------------------------------------------------
        // Feed to Weka J48
        --------------------------------------------------------------------------------------------------------------*/

        System.out.println();
        System.out.println("Feed Matrix to Decision Tree, write down the Classification Tree model, and extract the shapelet data points.");
        ArrayList<String> listOfMatrix = new ArrayList<>();
        for(String thisPath: listOfDM){
            listOfMatrix.add(thisPath);
        }
        for(String thisPath: listOfFM){
            listOfMatrix.add(thisPath);
        }
        for(String thisPath: listOfFDM){
            listOfMatrix.add(thisPath);
        }
        System.out.println("--There are :" + listOfMatrix.size() + " matrix constructed");

        for(String path: listOfMatrix){
            File MatrixPath = new File(path);
            System.out.print("--" + MatrixPath.getName() + "; ");

            CSVLoader loader = new CSVLoader();
            loader.setSource(MatrixPath);
            Instances dataInstance = loader.getDataSet();
            dataInstance.setClassIndex(dataInstance.numAttributes() - 1);

            String writeThis = "";

            System.out.print("Classifying. ");
            // build classifier
            String[] options = weka.core.Utils.splitOptions("-C 0.25 -M 2");
            J48 classifier = new J48();
            classifier.setOptions(options);
            classifier.buildClassifier(dataInstance);

            System.out.print("Save tree to txt. ");
            //save the J48 tree to a txt file
            String writePath = MatrixPath.getParent() + "/tree_" + MatrixPath.getName().substring(0,MatrixPath.getName().length()-4) + ".txt";
            BufferedWriter writer = new BufferedWriter(new FileWriter(writePath));
            writer.write(classifier.toString());

            // get the shapelet id
            ArrayList<String> bmlShapes = new ArrayList<>();
            ArrayList<String> bmlShapeValue = new ArrayList<>();
            for (String thisLine: classifier.graph().split("\\r?\\n")){
                if(thisLine.contains("<=")){
                    // value
                    bmlShapeValue.add(thisLine.substring(thisLine.indexOf("= ")+2,thisLine.length()-2));
                } else if(!(thisLine.contains("->") || thisLine.contains("{") || thisLine.contains("}") || thisLine.contains("shape"))){
                    // id
                    bmlShapes.add(thisLine.substring(thisLine.indexOf("=")+2,thisLine.length()-3));
                }
            }

            writeThis = "";
            for (String theId: bmlShapes){
                writeThis += theId + ",";
            }
            writer.write(System.lineSeparator());
            writer.write("Shapelet Id:" + System.lineSeparator());
            writer.write(writeThis + System.lineSeparator());
            writeThis = "";
            for (String theValue: bmlShapeValue){
                writeThis += theValue + ",";
            }
            writer.write(System.lineSeparator());
            writer.write("Shapelet Value:" + System.lineSeparator());
            writer.write(writeThis + System.lineSeparator());
            writeThis = "";
            writer.close();

            System.out.print("Searching the shapelets. ");
            // extract shapelet data points
            ArrayList<float[]> bmlDataPoints = new ArrayList<>();
            for (String thisShape: bmlShapes){
                String[] splitter = thisShape.split("_"); // tsNumber_colNumber_winSize_slidePts_uniqueNumber

                // get one time-series from the instance according to its tsNum and colNum
                float[] oneColumn = tsCollection.get(Integer.valueOf(splitter[0])).getData().get(Integer.valueOf(splitter[1]));

                // segment the time-series according to winSize and slidePts
                ArrayList<float[]> data = timeSeriesSegmentation.slideWindow(Integer.valueOf(splitter[2]), Integer.valueOf(splitter[3]), oneColumn);

                // find the shapelet between the segment
                int featureCount=1;
                boolean found = false;
                for(float[] thisData: data){
                    if(featureCount == Integer.valueOf(splitter[4])){
                        bmlDataPoints.add(thisData);
                        found = true; // found it!, extract and quit!
                    }
                    if(found) break;
                    featureCount++;
                }
            }

            System.out.println("Writing down the shapelets.");
            // put the id and its data points into csv file.
            writePath = MatrixPath.getParent() + "/shape_" + MatrixPath.getName();
            CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, false), ',', CSVWriter.NO_QUOTE_CHARACTER);

            // write down the id
            for(String shapeName: bmlShapes) writeThis += shapeName + ",";
            csvWriter.writeNext(writeThis.split(","));
            writeThis="";

            // write down the values. ASSUMING all the shapelet have the same length
            for(int i = 0; i < bmlDataPoints.get(0).length; i++){
                for(float[] dataPoint: bmlDataPoints){
                    writeThis += dataPoint[i] + ",";
                }
                csvWriter.writeNext(writeThis.split(","));
                writeThis="";
            }
            csvWriter.close();
        }
        System.out.println("DONE. " + formatter.format(Instant.now()));

        Instant stop = Instant.now();
        Duration d = Duration.between( start , stop ) ;
        long minutesPart = d.toMinutes();
        long secondsPart = d.minusMinutes( minutesPart ).getSeconds() ;

        System.out.println();
        System.out.println( "Interval: "  + formatter.format(start) + " - " + formatter.format(stop) );
        System.out.println( "Elapsed: " + minutesPart + "M " + secondsPart + "S" );
    }

    private static void prepareEnvironment(){
        if(new File(filteredDataPath).exists()){
            for(File file: new File(filteredDataPath).listFiles())
                if (!file.isDirectory()) file.delete();
        } else {
            new File(filteredDataPath).mkdir();
        }

        if(new File(transposedPath).exists()){
            for(File file: new File(transposedPath).listFiles())
                if (!file.isDirectory()) file.delete();
        } else {
            new File(transposedPath).mkdir();
        }
    }

    private static void filterTimeSeries(ArrayList<String> listOfFiles) throws IOException {
        // create path bank of template from dir
        try(Stream<Path> paths = Files.walk(Paths.get(rawDataPath)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //todo: read all csv then prep var sets for next step
        for (String thisFilePath: listOfFiles){
            File rawSource = new File(thisFilePath);
            String writePath = filteredDataPath + "/F_" + rawSource.getName();
            CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, new File(writePath).exists()), ',', CSVWriter.NO_QUOTE_CHARACTER);

            //todo: read data from csv file
            CSVReader reader = new CSVReader(new FileReader(rawSource)); // read template csv and store to variable
            headerName = reader.readNext();
            csvWriter.writeNext(headerName);
            colCount = headerName.length;

            // variable used
            String[] nextLine;
            String thisLineValue="";

            //todo: apply filter

            if(filterMethod==0 || filterMethod==1){
                MeanMedian mmAccX = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmAccY = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmAccZ = new MeanMedian(filterWindowSize, filterMethod);

                MeanMedian mmGyroX = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmGyroY = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmGyroZ = new MeanMedian(filterWindowSize, filterMethod);

                while ((nextLine = reader.readNext()) != null) {
                    thisLineValue += mmAccX.updateSample(Float.valueOf(nextLine[0])) + ",";
                    thisLineValue += mmAccY.updateSample(Float.valueOf(nextLine[1])) + ",";
                    thisLineValue += mmAccZ.updateSample(Float.valueOf(nextLine[2])) + ",";
                    thisLineValue += mmGyroX.updateSample(Float.valueOf(nextLine[3])) + ",";
                    thisLineValue += mmGyroY.updateSample(Float.valueOf(nextLine[4])) + ",";
                    thisLineValue += mmGyroZ.updateSample(Float.valueOf(nextLine[5])) + ",";

                    for (int i=6; i<=colCount-1; i++){
                        thisLineValue += nextLine[i] + ((i!=colCount-1) ? "," : "");
                    }

                    csvWriter.writeNext(thisLineValue.split(","));
                    thisLineValue="";
                }
            } else {
                float[] filteredAcc = new float[3];

                while ((nextLine = reader.readNext()) != null) {
                    filteredAcc = LowPass.filter(Arrays.copyOfRange(nextLine, 0, 3) ,filteredAcc);
                    thisLineValue = filteredAcc[0] + "," + filteredAcc[1] + "," + filteredAcc[2] + ",";
                    for (int i=3; i<=colCount-1; i++){
                        thisLineValue += nextLine[i] + ((i!=colCount-1) ? "," : "");
                    }

                    csvWriter.writeNext(thisLineValue.split(","));
                    thisLineValue="";
                }
            }
            csvWriter.close();
        }

        listOfFiles.clear();
        try(Stream<Path> paths = Files.walk(Paths.get(filteredDataPath)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void ReadTransposeRenameFiles(String fileToTranspose, String classLabeling) throws IOException{
        File theCsvFile = new File(fileToTranspose);
        System.out.print(theCsvFile.getName()+",");
        CSVWriter csvWriter;

        // Before transpose, add class label at last line
        int recordCount = 0;
        if(!classLabeling.isEmpty()){
            csvWriter = new CSVWriter(new FileWriter(theCsvFile, true), ',', CSVWriter.NO_QUOTE_CHARACTER);
            String[] classX = classLabeling.split(",");
            recordCount = classX.length - 1;
            csvWriter.writeNext(classX);
            csvWriter.close();
        } else {
            //count the rows
            CSVReader csvReader = new CSVReader(new FileReader(theCsvFile)); // read template csv and store to variable
//            while(csvReader.readNext() != null){
//                recordCount++;
//            }
            recordCount = csvReader.readNext().length - 1;
            csvReader.close();
        }

        File tempFile = new File(theCsvFile.getParent() + "/temp.csv");
        csvWriter = new CSVWriter(new FileWriter(tempFile, false), ',', CSVWriter.NO_QUOTE_CHARACTER); // change to this to append: new File(writePath).exists()));

        int nowCol = 0;
        while(nowCol <= recordCount){
            CSVReader csvReader = new CSVReader(new FileReader(theCsvFile)); // read template csv and store to variable
            String[] nextLine;
            StringBuilder lineCreator = new StringBuilder("");

            while((nextLine = csvReader.readNext()) != null){
                lineCreator.append(nextLine[nowCol]).append(",");
            }

            csvWriter.writeNext(lineCreator.toString().split(","));
            csvReader.close();

            nowCol++;
        }
        csvWriter.close();

        // delete original file and rename temp file to original
        FileWorker.RenameFile(theCsvFile, tempFile);
    }

    private static void ReadTransposeRenameFiles(ArrayList<String> listOfFiles, String classLabeling) throws IOException {
        for (String thisFilePath : listOfFiles) {
            ReadTransposeRenameFiles(thisFilePath, classLabeling);
        }
    }

    public static void ReadAndTransposeToCollection(ArrayList<String> listOfFiles, ArrayList<tsSegment> tsCollection) throws IOException{
        // read all csv then prep var sets for next step
        for (String thisFilePath: listOfFiles){
            File theCsvFile = new File(thisFilePath);

            // read data from csv file
            CSVReader csvFromVictim = new CSVReader(new FileReader(theCsvFile)); // read template csv and store to variable
            headerName = csvFromVictim.readNext();
            colCount = headerName.length - 1;
            List<String[]> victimValues = csvFromVictim.readAll();

            if(victimValues.size() < minRowCount || minRowCount == 0) {
                minRowCount = victimValues.size();
//                System.out.println("--file " + thisFilePath + " has row count of : " + (victimValues.size() +1));
            }

            // transpose row to column
            ArrayList<float[]> oneCSVdata = transpose_toFloat(victimValues, colCount);

            tsSegment tempTS = new tsSegment();
            tempTS.setFileNumber(fileCount);
            tempTS.setData(oneCSVdata);
            tempTS.setLabel(getSegmentLabel(victimValues, colCount));
            tsCollection.add(tempTS);

            fileCount++;
        }
    }

    private static ArrayList<tsSegment> segmentTimeSeries(ArrayList<tsSegment> tsCollection, int winSize, int slidePoint) throws IOException {
        ArrayList<tsSegment> segCollection = new ArrayList<>();

        //todo: dynamic segmentation
        int cCol=0;
        for(tsSegment oneFile: tsCollection){
            for(float[] oneColumn: oneFile.getData()){

                if (oneColumn.length < winSize) break;

                tsSegment tempSeg = new tsSegment();

                tempSeg.setFileNumber(oneFile.getFileNumber());
                tempSeg.setColumnNumber(cCol);
                tempSeg.setData(timeSeriesSegmentation.slideWindow(winSize, slidePoint, oneColumn)); // one tempSeg = one column data
                tempSeg.setLabel(headerName[cCol]);

                segCollection.add(tempSeg);
                cCol++;
            }
            cCol = 0;
        }

        return segCollection;
    }

    private static void calculateDistance(ArrayList<tsSegment> tsCollection, ArrayList<tsSegment> segCollection, int winSize, int slidePoint, String ParentPath) throws IOException {
        //todo: distance measurement
        String DistancePath = ParentPath + "/" + fnDistanceMatrix + "_" + winSize + ".csv";
        String IdPath = ParentPath + "/" + fnIdToRepo + "_" + winSize + ".csv";
        String IdRepoPath = ParentPath + "/" + fnRepo + "_" + winSize + ".csv";
        // automatically append to continue writing on same file if windowSize is not yet incremented
        CSVWriter DistanceWriter = new CSVWriter(new FileWriter(DistancePath, new File(DistancePath).exists()), ',', CSVWriter.NO_QUOTE_CHARACTER);
        CSVWriter IdWriter = new CSVWriter(new FileWriter(IdPath, new File(IdPath).exists()), ',', CSVWriter.NO_QUOTE_CHARACTER);
        CSVWriter IdRepoWriter = new CSVWriter(new FileWriter(IdRepoPath, new File(IdRepoPath).exists()));

        int featureCount=1, counterIdx = 1, lastColNumber = 0;
        for(tsSegment thisSeg: segCollection) { // one thisSeg only consist of one column
            for(float[] thisSegData: thisSeg.getData()) { // get list of segments from one column

                if(thisSeg.getColumnNumber() != lastColNumber) {
                    featureCount = 1;
                    lastColNumber = thisSeg.getColumnNumber();
                }

                // segment labelling: tsNumber_colNumber_winSize_slidePts_uniqueNumber
                String valDistance = thisSeg.getFileNumber() + "_" + thisSeg.getColumnNumber() + "_" + winSize + "_" + slidePoint + "_" + featureCount + ",";
                String valId = valDistance;

                //compare each segment with same column segments but only with segments from different time series data
                for(tsSegment thisTs: tsCollection) {
                    if(thisSeg.getFileNumber() == thisTs.getFileNumber()){ //todo: fill distance measured with 0 if seg came from same time series
                        valDistance += "0,";
                        valId += "0,";
                    } else {
                        Predicate<tsSegment> filterSameWithTs = tsSegment -> tsSegment.getFileNumber() == thisTs.getFileNumber() && tsSegment.getColumnNumber() == thisSeg.getColumnNumber();
                        List<tsSegment> filteredList = segCollection.stream().filter(filterSameWithTs).collect(Collectors.toList());

                        double leastDistance = 0;
                        String[] leastSegment = new String[2];
                        for(float[] segFromSameTsData : filteredList.get(0).getData()){
                            double thisDistance;
                            if(distanceMeasurement == 0) {
                                DTW calcDTW = new DTW(thisSegData, segFromSameTsData);
                                thisDistance = calcDTW.getWarpingDistance();
                            } else if (distanceMeasurement == 1) {
                                EuclideanDistance calcED = new EuclideanDistance(thisSegData, segFromSameTsData);
                                thisDistance = calcED.getTotalDistance();
                            } else {
                                PearsonCorrelation calcPCC = new PearsonCorrelation(thisSegData, segFromSameTsData);
                                thisDistance = calcPCC.getTotalDistance();
                            }

                            if(leastDistance==0 || thisDistance<leastDistance){
                                leastDistance = thisDistance;
                                leastSegment[1] = Arrays.toString(segFromSameTsData).replace("[","").replace("]","");
                            }
                        }

                        valDistance += leastDistance + ",";
                        valId += counterIdx + "-" + slidePoint + ",";
                        leastSegment[0] = counterIdx + "-" + slidePoint;
                        IdRepoWriter.writeNext(leastSegment);

                        counterIdx++;
                    }
                }

                //write to CSV after finish
                if(!valDistance.isEmpty()){
                    valDistance = valDistance.substring(0, valDistance.length()-1);
                    valId = valId.substring(0, valId.length()-1);

                    DistanceWriter.writeNext(valDistance.split(","));
                    IdWriter.writeNext(valId.split(","));
                }
                featureCount++;
            }
        }
        DistanceWriter.close();
        IdWriter.close();
        IdRepoWriter.close();
        segCollection.clear();
    }

    private static void calculateFeatures(ArrayList<tsSegment> tsCollection, ArrayList<tsSegment> segCollection, int winSize, int slidePoint, String ParentPath) throws IOException {

        // assumption : there are 6 axes, segments created from one time series will have same number of segments across all axes

        for(tsSegment thisTs : tsCollection){
            String newParentPath = ParentPath + "/" + fnDistanceMatrix + "_" + winSize;
            new File(newParentPath).mkdir();

            String writePath = newParentPath + "/" + thisTs.getFileNumber() + "_" + slidePoint + "_" + thisTs.getLabel() + ".csv";
            // automatically append to continue writing on same file if windowSize is not yet incremented
            CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, new File(writePath).exists()), ',', CSVWriter.NO_QUOTE_CHARACTER);

            // filter
            Predicate<tsSegment> filterSameWithTs = tsSegment -> tsSegment.getFileNumber() == thisTs.getFileNumber();
            List<tsSegment> filteredList = segCollection.stream().filter(filterSameWithTs).collect(Collectors.toList()); // one list have 6 Segment.tsSegment, one for each axes

            // init label with format : tsNumber_winSize_slidePts_sensor_axis_featureName
            ArrayList<String> featureBuilder = new ArrayList<>();
            for(String featName: FeatureExtract.getFeatureNameList()) { // class label for accelerometer
                featureBuilder.add(thisTs.getFileNumber() + "_" + winSize + "_" + slidePoint + "_" + 0 + "_" + featName + ",");
            }
            for(String featName: FeatureExtract.getFeatureNameList()) { // class label for gyroscope
                featureBuilder.add(thisTs.getFileNumber() + "_" + winSize + "_" + slidePoint + "_" + 1 + "_" + featName + ",");
            }

            int countSegment = filteredList.get(0).getData().size();
            for(int i = 0; i <= countSegment - 1; ++i) {

                // compute features for each 3 segments (3 axes) for each sensor value
                FeatureExtract accelFeatures = new FeatureExtract(filteredList.get(0).getData().get(i), filteredList.get(1).getData().get(i), filteredList.get(2).getData().get(i));
                FeatureExtract gyroFeatures = new FeatureExtract(filteredList.get(3).getData().get(i), filteredList.get(4).getData().get(i), filteredList.get(5).getData().get(i));

                // update builder with feature values
                ArrayList<Float> accelFeatValues = accelFeatures.getFeaturesValue();
                ArrayList<Float> gyroFeatValues = gyroFeatures.getFeaturesValue();
                for(int featIdx=0; featIdx<= accelFeatValues.size() -1; featIdx++){
                    featureBuilder.set(featIdx, featureBuilder.get(featIdx) + accelFeatValues.get(featIdx) + ",");
                }
                for(int featIdx=0; featIdx<= gyroFeatValues.size() - 1; featIdx++){
                    featureBuilder.set(accelFeatValues.size() + featIdx, featureBuilder.get(accelFeatValues.size() + featIdx) + gyroFeatValues.get(featIdx) + ",");
                }
            }

            // write down the values to csv
            for(String oneLine: featureBuilder)
                csvWriter.writeNext(oneLine.substring(0, oneLine.length() - 1).split(","));

            csvWriter.close();
        }
        segCollection.clear();
    }

    private static void keepOnlyTopNCandidate(ArrayList<String> listOfFiles, int topN) throws Exception {
        for (String distancePath : listOfFiles) {
            File distanceFile = new File(distancePath);

            // check the file size
            float fileSizeInMb = distanceFile.length()/(1024 * 1024);

            if(fileSizeInMb>300) { // if file is more than 300mb, slice then rank. Else, do it in one go.
                System.out.println("--file is too big. slicing before ranking");
                ArrayList<String> chunkList = new ArrayList<>();
                int oneChunk =29999; // slice the matrix into a chunk of 30k columns

                CSVReader csvReader = new CSVReader(new FileReader(distanceFile));
                String[] nextLine;

                System.out.print("--Processing row: ");
                int nowRow = 1;
                while ((nextLine = csvReader.readNext()) != null) { // write each chunk in turn into csv, each with append parameter.
                    System.out.print(nowRow + ","); nowRow++;

                    boolean sliced = false; // flag to show if one file has been sliced to the end
                    int now = 0, fileNum = 1;

                    while (!sliced){
                        String writePath = distanceFile.getParent() + "/chunk-" + fileNum + ".csv";
                        if(!chunkList.contains(writePath)) chunkList.add(writePath);

                        CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, new File(writePath).exists()), ',', CSVWriter.NO_QUOTE_CHARACTER);
                        String writeNextLine = "";

                        int maxRow = (now+oneChunk>nextLine.length-1) ? (nextLine.length-2)-now : oneChunk;
                        for(int i = 0; i<=maxRow; i++){
                            writeNextLine += nextLine[now] + ",";
                            now++;
                        }
                        writeNextLine += nextLine[nextLine.length-1];
                        csvWriter.writeNext(writeNextLine.split(","));
                        csvWriter.close();

                        if(now >= nextLine.length-1) sliced=true;
                        fileNum++;
                    }
                }
                System.out.println("fin");
                csvReader.close(); // finished slicing

//                chunkList.add("./data/dmTransposed/chunk-1.csv");
//                chunkList.add("./data/dmTransposed/chunk-2.csv");
//                chunkList.add("./data/dmTransposed/chunk-3.csv");
//                chunkList.add("./data/dmTransposed/chunk-4.csv");
//                chunkList.add("./data/dmTransposed/chunk-5.csv");

                System.out.println("--Rank the slices");
                ArrayList<String> listOfTopN;
                for(String slice: chunkList){
                    // rank each slice
                    File sliceFile = new File(slice);
                    listOfTopN = new ArrayList<>(Arrays.asList(wekaRanker.applyInfoGain(sliceFile, topN)));
                    int[] idxTopN = matrixTopN.GetTopIndex(listOfTopN, sliceFile);

                    // filter each slice based on rank. keep only last-index-column on last slice-file
                    if(Objects.equals(slice, chunkList.get(chunkList.size() - 1)))
                        matrixTopN.KeepListedOnly(idxTopN, sliceFile, true);
                    else
                        matrixTopN.KeepListedOnly(idxTopN, sliceFile, false);
                }

                System.out.println("--Merge all slices into one file then rank it again");
                // merge into one file. should be enough unless there are more than 8 slices.. which mean original file is having 240k columns before..
                String writePath = distanceFile.getParent() + "/merged.csv";
                FileWorker.MergeFileHorizontally(chunkList, writePath, false);

                // redo rank-filter and put it down to final.
                File mergedFile = new File(writePath);
                listOfTopN = new ArrayList<>(Arrays.asList(wekaRanker.applyInfoGain(mergedFile, topN)));
                int[] idxTopN = matrixTopN.GetTopIndex(listOfTopN, mergedFile);
                matrixTopN.KeepListedOnly(idxTopN, mergedFile, true);

                System.out.println("--Get top-N index and apply to idFile");
                // refill listOfTopN and idxTopN with index from merged file compared with distance file
                csvReader = new CSVReader(new FileReader(mergedFile));
                listOfTopN = new ArrayList<>(Arrays.asList(csvReader.readNext()));
                csvReader.close();
                idxTopN = matrixTopN.GetTopIndex(listOfTopN, distanceFile);

                // distanceFile is now useless. delete it, rename mergedFile to distanceFile
                FileWorker.RenameFile(distanceFile, mergedFile);

                // at last got the index of top-N, time to save only top-N from distanceFile and idFile
                String idPath = transposedPath + "/" + fnIdToRepo + "_" + distanceFile.getName().split("_")[1];
                File idFile = new File(idPath);
                matrixTopN.KeepListedOnly(idxTopN, idFile, true);

            } else {
                /*--------------------------------------------------------------------------------------------------------------
                // Compute top-N attributes using Gain Ratio / Information Gain from Distance Matrix, Get index position of top-N
                --------------------------------------------------------------------------------------------------------------*/
                ArrayList<String> listOfTopN;
                if(RankingMethod==0)
                    listOfTopN = new ArrayList<>(Arrays.asList(wekaRanker.applyInfoGain(distanceFile, topN)));
                else
                    listOfTopN = new ArrayList<>(Arrays.asList(wekaRanker.applyGainRatio(distanceFile, topN)));

                /*--------------------------------------------------------------------------------------------------------------
                // Remove attributes not in top-N
                --------------------------------------------------------------------------------------------------------------*/
                int[] idxTopN = matrixTopN.GetTopIndex(listOfTopN, distanceFile);

                String idPath = transposedPath + "/" + fnIdToRepo + "_" + distanceFile.getName().split("_")[1];
                File idFile = new File(idPath);
                matrixTopN.KeepListedOnly(idxTopN, distanceFile, idFile);
            }
        }
    }

    private static void calculateDiffOfFeatureValue(ArrayList<tsSegment> tsCollection, ArrayList<String> listOfFilesId, ArrayList<String> listOfFM, String columnForClassLabel) throws IOException {

        /*--------------------------------------------------------------------------------------------------------------
        // re-inverse all id_x.csv. to make it easier to process for computing the difference of feature-value later
        --------------------------------------------------------------------------------------------------------------*/

        System.out.print("--Transpose back Id files to improper format, for easier process: ");
        ReadTransposeRenameFiles(listOfFilesId,"");
        System.out.println(" finish.");

        /*--------------------------------------------------------------------------------------------------------------
        // calculate feature and its differences with least distance!!
        // read id_x.csv file, segment, and read the id it relate with at next values
        // read repo_x.csv file, trace id above from here
        --------------------------------------------------------------------------------------------------------------*/

        System.out.print("--Finding feature difference: ");
        ArrayList<tsSegment> thisDatasetSegments = new ArrayList<>();

        for (String idPath: listOfFilesId){ // loop for all window id files.
            File csvId = new File(idPath);
            String thisRepoPath = transposedPath + "/" + fnRepo + "_" + csvId.getName().split("_")[1];
            File csvRepo = new File(thisRepoPath);

            System.out.print(fnFeatureMatrix + "_" + csvId.getName().split("_")[1] + ",");

            CSVReader readId = new CSVReader(new FileReader(csvId));
            CSVReader readRepo = new CSVReader(new FileReader(csvRepo));
            String[] nextIdLine, nextRepoLine;
            ArrayList<String> valueToWrite = new ArrayList<>();

            String fmPath = transposedPath + "/" + fnFeatureMatrix + "_" + csvId.getName().split("_")[1];
            CSVWriter writeThisFM = new CSVWriter(new FileWriter(fmPath, false), ',', CSVWriter.NO_QUOTE_CHARACTER);

            String lastFileNumber = "", lastWinSize = "", lastSlideSize = "";
            while ((nextIdLine = readId.readNext()) != null) { // loop for each row of this window id files.
                if(nextIdLine[0].equals("classLabel")) break;
                String[] splitter = nextIdLine[0].split("_"); // id-0 is shapelet id : file_col_win_slide_num

                for(String featName : ExtractStatisticFeatures.getFeatureName()){ // set-up the feature naming : shapelet_id + feature
                    valueToWrite.add(nextIdLine[0] + "_" + featName); // each line will be continuously updated later
                }

                if(!lastFileNumber.equals(splitter[0]) || !lastWinSize.equals(splitter[2]) || !lastSlideSize.equals(splitter[3])) {
                    Predicate<tsSegment> filterSameWithTs = tsSegment -> tsSegment.getFileNumber() == Integer.valueOf(splitter[0]);
                    List<tsSegment> filteredList = tsCollection.stream().filter(filterSameWithTs).collect(Collectors.toList());

                    thisDatasetSegments = segmentTimeSeries((ArrayList<tsSegment>) filteredList, Integer.valueOf(splitter[2]), Integer.valueOf(splitter[3]));
                    lastWinSize = splitter[2];
                    lastSlideSize = splitter[3];
                }

                float[] thisFeatureShape = new float[Integer.parseInt(lastWinSize)], leastSegmentShape = new float[Integer.parseInt(lastWinSize)];

                // find value of segment A from id (shapelet)
                int featureCount=1; // FIXME: make it continuous counting
                for(tsSegment thisSeg: thisDatasetSegments) {
                    if(thisSeg.getColumnNumber() != Integer.valueOf(splitter[1])) continue; // different column, skip
                    for (float[] thisSegData : thisSeg.getData()) { // loop all segments from this column
                        if(featureCount == Integer.valueOf(splitter[4])){
                            thisFeatureShape = thisSegData;
                            break; // break for this loop once shapelet is found
                        }else{
                            featureCount++; // continue loop til shapelet is found
                        }
                    }
                    break; // break segment loop after duplicate the shapelet
                }

                for (int i = 1; i<nextIdLine.length; i++){ // loop for each column (has id inside) from this row, start from id-1
                    if(nextIdLine[i].equals("0")) {
                        for(int j = 0; j<ExtractStatisticFeatures.getFeatureCount(); j++){
                            String tempVal = valueToWrite.get(j) + ",0";
                            // one row has : feat_id, diff_val of 1 feature with t1,t2,t3,...
                            valueToWrite.set(j, tempVal);
                        }
                        continue; // valueId=0, means distance of segment A and B are from the same dataset. no need to compute
                    }

                    // find value of segment B from repo (segment with least distance)
                    boolean foundFromRepo = false;
                    while(!foundFromRepo){
                        nextRepoLine = readRepo.readNext(); // index 0 = id, index 1 = value
                        if(nextRepoLine[0].equals(nextIdLine[i])){
                            // is in String, need to convert it into float first
                            leastSegmentShape = StringToFloat(nextRepoLine[1].split(","));
                            foundFromRepo = true;
                        }
                    }

                    // compute feature values
                    ArrayList<Float> featureFromShape = ExtractStatisticFeatures.getAll(thisFeatureShape);
                    ArrayList<Float> featureFromSegment = ExtractStatisticFeatures.getAll(leastSegmentShape);

                    // compute each feature value difference
                    for(int j = 0; j<ExtractStatisticFeatures.getFeatureCount(); j++){
                        String tempVal = valueToWrite.get(j) + "," + (featureFromShape.get(j) - featureFromSegment.get(j));
                        // one row has : feat_id, diff_val of 1 feature with t1,t2,t3,...
                        valueToWrite.set(j, tempVal);
                    }
                }
                for (String tempVal : valueToWrite){
                    writeThisFM.writeNext(tempVal.split(","));
                }
                valueToWrite.clear();
            }
            listOfFM.add(fmPath);
            writeThisFM.close();
            readId.close();
        }
        System.out.println(" finish.");

        System.out.print("--Transpose the csv, make it back to proper matrix format: ");
        for (String thisFeatMatrix : listOfFM) {
            File theCsvFile = new File(thisFeatMatrix);

            // duplicate this file to be used as the base for merging feature and distance candidates later
            File featWoClass = new File(transposedPath + "/" + fnFeatureNoClass + "_" + theCsvFile.getName().split("_")[1]);
            Files.copy(theCsvFile.toPath(), featWoClass.toPath());

            // ---------------------------------------------------- Processing FeatureMatrix
            ReadTransposeRenameFiles(thisFeatMatrix, columnForClassLabel);

            // ---------------------------------------------------- Processing FeatureMatrix without classLabel
            ReadTransposeRenameFiles(featWoClass.getPath(), "");
        }

        System.out.println(" finish.");
    }

    private static String[] getHeader(String[] original){
        String[] newArray = new String[original.length + 1];
        System.arraycopy(original, 0, newArray, 0, original.length);
        newArray[original.length] = "classLabel";
        return  newArray;
    }

    public static ArrayList<float[]> transpose_toFloat(List<String[]> thisIsShit, int colCount){
        ArrayList<float[]> notShit = new ArrayList<>();

        for(int i = 0; i <= colCount-1; ++i){ //iterate column, not including the last column (is a string for label)
            float[] tempDoubleArrayList = new float[thisIsShit.size()];

            int rowCount = 0;

            for (String[] lineTokens : thisIsShit) {
                if (lineTokens[i].trim().length() == 0){
                    float[] changeArraySize = Arrays.copyOfRange(tempDoubleArrayList, 0,  rowCount);
                    tempDoubleArrayList = new float[rowCount];
                    System.arraycopy(changeArraySize, 0, tempDoubleArrayList, 0, rowCount);

                    break;
                }

                tempDoubleArrayList[rowCount] = Float.valueOf(lineTokens[i]);
                rowCount++;
            }
            notShit.add(tempDoubleArrayList);
        }

        return notShit;
    }

    public static ArrayList<String[]> transpose_toString(List<String[]> thisIsShit, int colCount){
        ArrayList<String[]> notShit = new ArrayList<>();

        for(int i = 0; i <= colCount-1; ++i){ //iterate column, not including the last column (is a string for label)
            String[] tempDoubleArrayList = new String[thisIsShit.size()];
            int rowCount = 0;

            for (String[] lineTokens : thisIsShit) {
                if (lineTokens[i].trim().length() == 0){
                    String[] changeArraySize = Arrays.copyOfRange(tempDoubleArrayList, 0,  rowCount);
                    tempDoubleArrayList = new String[rowCount];
                    System.arraycopy(changeArraySize, 0, tempDoubleArrayList, 0, rowCount);

                    break;
                }

                tempDoubleArrayList[rowCount] = lineTokens[i];
                rowCount++;
            }
            notShit.add(tempDoubleArrayList);
        }

        return notShit;
    }

    private static int countData(float[] data){
        int rowCounted = 0;

        for(float ignored : data){
            rowCounted++;
        }

        return rowCounted;
    }

    public static String getSegmentLabel(List<String[]> thisIsShit, int colCount){
        String label = "";
        for (String[] lineTokens : thisIsShit) {
            label = lineTokens[colCount];
            break;
        }

        return label;
    }

    private static float[] StringToFloat(String[] problem){
        float[] newVar = new float[problem.length];

        for (int i = 0; i<problem.length; i++){
            newVar[i] = Float.valueOf(problem[i]);
        }

        return newVar;
    }
}
