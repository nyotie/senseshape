import Segment.timeSeriesSegmentation;
import Segment.tsSegment;
import com.opencsv.CSVWriter;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.CSVLoader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Created by nyotie on 14-Jul-17.
 */
public class ClassifyThenExtract {
    private static ArrayList<String> listOfFilesRaw = new ArrayList<>();
    private static ArrayList<tsSegment> tsCollection = new ArrayList<>(); // ga penting
    private static String rawDataPath="./data/filtered";
//    private static File path = new File("./data/distance_30.csv");
//    static File path = new File("./data/feature_30.csv");
    static File path = new File("./data/FeatDist_30.csv");

    public static void main(String[] args) throws Exception {
        if(listOfFilesRaw.size() == 0){
            try(Stream<Path> paths = Files.walk(Paths.get(rawDataPath)).filter(Files::isRegularFile)) { // get the path from raw if not filtered first
                paths.forEach(filePath -> listOfFilesRaw.add(filePath.toString()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        calculateDistanceMatrix.ReadAndTransposeToCollection(listOfFilesRaw, tsCollection);

        CSVLoader loader = new CSVLoader();
        loader.setSource(path);
        Instances dataInstance = loader.getDataSet();
        dataInstance.setClassIndex(dataInstance.numAttributes() - 1);

        String writeThis;

        // build classifier
        String[] options = weka.core.Utils.splitOptions("-C 0.25 -M 2");
        J48 classifier = new J48();
        classifier.setOptions(options);
        classifier.buildClassifier(dataInstance);

        System.out.println("Here come shits!");

        //save the J48 tree to a txt file
        String writePath = path.getParent() + "/tree_" + path.getName().substring(0,path.getName().length()-4) + ".txt";
        BufferedWriter writer = new BufferedWriter(new FileWriter(writePath));
        writer.write(classifier.toString());

        // get the shapelet id
        ArrayList<String> bmlShapes = new ArrayList<>();
        ArrayList<String> bmlShapeValue = new ArrayList<>();
        for (String thisLine: classifier.graph().split("\\r?\\n")){
            if(thisLine.contains("<=")){
                // value
                bmlShapeValue.add(thisLine.substring(thisLine.indexOf("= ")+2,thisLine.length()-2));
            } else if(!(thisLine.contains("->") || thisLine.contains("{") || thisLine.contains("}") || thisLine.contains("shape"))){
                // id
                bmlShapes.add(thisLine.substring(thisLine.indexOf("=")+2,thisLine.length()-3));
            }
        }

        writeThis = "";
        for (String theId: bmlShapes){
            writeThis += theId + ",";
        }
        writer.write(System.lineSeparator());
        writer.write("Shapelet Id:" + System.lineSeparator());
        writer.write(writeThis + System.lineSeparator());
        writeThis = "";
        for (String theValue: bmlShapeValue){
            writeThis += theValue + ",";
        }
        writer.write(System.lineSeparator());
        writer.write("Shapelet Value:" + System.lineSeparator());
        writer.write(writeThis + System.lineSeparator());
        writeThis = "";
        writer.close();


        // extract shapelet data points
        ArrayList<float[]> bmlDataPoints = new ArrayList<>();
        for (String thisShape: bmlShapes){
            String[] splitter = thisShape.split("_"); // tsNumber_colNumber_winSize_slidePts_uniqueNumber

            // get one time-series from the instance according to its tsNum and colNum
            float[] oneColumn = tsCollection.get(Integer.valueOf(splitter[0])).getData().get(Integer.valueOf(splitter[1]));

            // segment the time-series according to winSize and slidePts
            ArrayList<float[]> data = timeSeriesSegmentation.slideWindow(Integer.valueOf(splitter[2]), Integer.valueOf(splitter[3]), oneColumn);

            // find the shapelet between the segment
            int featureCount=1;
            boolean found = false;
            for(float[] thisData: data){
                if(featureCount == Integer.valueOf(splitter[4])){
                    bmlDataPoints.add(thisData);
                    found = true; // found it!, extract and quit!
                }
                if(found) break;
                featureCount++;
            }
        }

        // put the id and its data points into csv file.
        writePath = path.getParent() + "/shape_" + path.getName();
        CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, false), ',', CSVWriter.NO_QUOTE_CHARACTER);

        // write down the id
        for(String shapeName: bmlShapes) writeThis += shapeName + ",";
        csvWriter.writeNext(writeThis.split(","));
        writeThis="";

        // write down the values. ASSUMING all the shapelet have the same length
        for(int i = 0; i < bmlDataPoints.get(0).length; i++){
            for(float[] dataPoint: bmlDataPoints){
                writeThis += dataPoint[i] + ",";
            }
            csvWriter.writeNext(writeThis.split(","));
            writeThis="";
        }
        csvWriter.close();
    }
}
