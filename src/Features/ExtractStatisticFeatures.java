package Features;

import org.apache.commons.math3.analysis.function.Abs;
import org.apache.commons.math3.analysis.function.Pow;
import org.apache.commons.math3.stat.descriptive.moment.Kurtosis;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Skewness;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import java.util.ArrayList;

/**
 * Created by garms on 08-Jun-17.
 *
 * Feature extraction only for one dimensional array with features are from statistic area only.
 */
public class ExtractStatisticFeatures {
    static float[] sample = null;

    // --------------------------------------------------------------------------- SETTER

    private static void setSample(float[] e1) {
        try{
            if(e1 == null) throw new NullPointerException();
            if(e1.length == 0) throw new IllegalArgumentException();

            sample = e1;
        } catch (NullPointerException err1){
            System.out.println("Can't handle null arrays");
        } catch (IllegalArgumentException err2){
            System.out.println("Can't handle zero-length arrays.");
        }
    }

    // --------------------------------------------------------------------------- CALLER

    public static int getFeatureCount(){
        return getFeatureName().length;
    }

    public static String[] getFeatureName(){
        String[] featureName = new String[12];

        featureName[0] = "Mean"; // Mean
        featureName[1] = "MAD"; // Mean Absolute Deviation
        featureName[2] = "Var"; // Variance
        featureName[3] = "SD"; // Standart Deviation
        featureName[4] = "Skew"; // Skewness
        featureName[5] = "Kurt"; // Kurtosis
        featureName[6] = "ZCR"; // Zero Crossing Rate
        featureName[7] = "MCR"; // Mean Crossing Rate
        featureName[8] = "AAD"; // Average Absolute Difference
        featureName[9] = "AAV"; // Average Absolute Value
        featureName[10] = "RMS"; // Root Mean Square
        featureName[11] = "Med"; // Median

        return featureName;
    }

    public static float getOneFeatureValue(float[] sample, String featureName){
        try{
            if(featureName.length() < 1) throw new Exception("Can't handle empty feature name");

            setSample(sample);

            switch(featureName){
                case "Mean":
                    return (float) getMean();
                case "MAD":
                    return (float) getMeanAbsDev();
                case "Var":
                    return (float) getVar();
                case "SD":
                    return (float) getStandardDeviation();
                case "Skew":
                    return (float) getSkew();
                case "Kurt":
                    return (float) getKurt();
                case "ZCR":
                    return (float) getZCR();
                case "MCR":
                    return (float) getMCR();
                case "AAD":
                    return (float) getAAD();
                case "AAV":
                    return (float) getAAV();
                case "RMS":
                    return (float) getRMS();
                case "Med":
                    return (float) getMedian();
            }
        } catch (Exception ex){
            System.out.println("Err: " + ex.toString());
        }

        return Float.parseFloat(null);
    }

    public static ArrayList<Float> getAll(float[] sample){
        setSample(sample);
        ArrayList<Float> FeaturesValue = new ArrayList<>();

        FeaturesValue.add((float) getMean());
        FeaturesValue.add((float) getMeanAbsDev());
        FeaturesValue.add((float) getVar());
        FeaturesValue.add((float) getStandardDeviation());
        FeaturesValue.add((float) getSkew());
        FeaturesValue.add((float) getKurt());
        FeaturesValue.add((float) getZCR());
        FeaturesValue.add((float) getMCR());
        FeaturesValue.add((float) getAAD());
        FeaturesValue.add((float) getAAV());
        FeaturesValue.add((float) getRMS());
        FeaturesValue.add((float) getMedian());

        return FeaturesValue;
    }

    // --------------------------------------------------------------------------- FEATURE CALCULATIONS

    private static double getMean() {
        Mean mean = new Mean();
        for (Float aDataset : sample) {
            mean.increment(aDataset);
        }
        return mean.getResult();
    }

    private static double getMeanAbsDev() {
        double mad = 0;
        double mean = getMean();

        for (Float aDataset : sample) {
            mad += Math.abs(aDataset - mean);
        }

        return mad/ sample.length;
    }

    private static double getVar() {
        Variance var = new Variance();
        for (Float aDataset : sample) {
            var.increment(aDataset);
        }
        return var.getResult();
    }

    private static double getStandardDeviation() {
        double variance = getVar();
        return Math.sqrt(variance);
    }

    private static double getSkew() {
        Skewness skew = new Skewness();
        for (Float aDataset : sample) {
            skew.increment(aDataset);
        }
        return skew.getResult();
    }

    private static double getKurt() {
        Kurtosis kurt = new Kurtosis();
        for (Float aDataset : sample) {
            kurt.increment(aDataset);
        }
        return kurt.getResult();
    }

    private static int getZCR() {
        int count = 0;
        for (int i = 0; i < sample.length - 1; i++) {
            if (sample[i] * sample[i + 1] < 0) {
                count++;
            }
        }
        return count;
    }

    private static int getMCR() {
        double mean = getMean();
        int count = 0;
        for (int i = 0; i < sample.length - 1; i++) {
            if ((sample[i] > mean && sample[i + 1] < mean)
                    || (sample[i] < mean && sample[i + 1] > mean)) {
                count++;
            }
        }
        return count;
    }

    private static double getAAD() {
        double dif = 0;
        Abs abs = new Abs();
        for (int i = 0; i < sample.length - 1; i++) {
            dif += abs.value(sample[i + 1] - sample[i]);
        }
        return dif;
    }

    private static double getAAV() {
        double sum = 0;
        Abs abs = new Abs();
        for (Float aDataset : sample) {
            sum += abs.value(aDataset);
        }
        return sum / sample.length;
    }

    private static double getRMS() {
        double sum = 0;
        Pow pow = new Pow();
        for (Float aDataset : sample) {
            sum += pow.value(aDataset, 2);
        }
        return pow.value(sum / sample.length, 0.5);
    }

    private static double getMedian(){
        if(sample.length % 2 == 0){
            // even
            int valuePos = sample.length / 2;
            return (sample[valuePos] + sample[valuePos+1])/2;
        } else {
            // odd
            return sample[sample.length / 2];
        }

    }

}
