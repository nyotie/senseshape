package Features;

import org.apache.commons.math3.analysis.function.Abs;
import org.apache.commons.math3.analysis.function.Pow;
import org.apache.commons.math3.stat.descriptive.moment.Kurtosis;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Skewness;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


public class FeatureExtract {

    private ArrayList<Float> FeaturesValue = new ArrayList<>();
    protected Element e1;
    protected Element e2;
    protected Element e3;

    // --------------------------------------------------------------------------- SETTER AND GETTER
    private Element getE1() {
        return e1;
    }

    public void setE1(float[] e1) {
        try{
            if(e1 == null) throw new NullPointerException();
            if(e1.length == 0) throw new IllegalArgumentException();

            ArrayList<Float> arrayList = new ArrayList<>();
            for(float num: e1) arrayList.add(num);

            this.e1 = new Element(arrayList);
        } catch (NullPointerException err1){
            System.out.println("Can't handle null arrays");
        } catch (IllegalArgumentException err2){
            System.out.println("Can't handle zero-length arrays.");
        }
    }

    private Element getE2() {
        return e2;
    }

    public void setE2(float[] e2) {
        try{
            if(e2 == null) throw new NullPointerException();
            if(e2.length == 0) throw new IllegalArgumentException();

            ArrayList<Float> arrayList = new ArrayList<>();
            for(float num: e2) arrayList.add(num);

            this.e2 = new Element(arrayList);
        } catch (NullPointerException err1){
            System.out.println("Can't handle null arrays");
        } catch (IllegalArgumentException err2){
            System.out.println("Can't handle zero-length arrays.");
        }
    }

    private Element getE3() {
        return e3;
    }

    public void setE3(float[] e3) {
        try{
            if(e3 == null) throw new NullPointerException();
            if(e3.length == 0) throw new IllegalArgumentException();

            ArrayList<Float> arrayList = new ArrayList<>();
            for(float num: e3) arrayList.add(num);

            this.e3 = new Element(arrayList);
        } catch (NullPointerException err1){
            System.out.println("Can't handle null arrays");
        } catch (IllegalArgumentException err2){
            System.out.println("Can't handle zero-length arrays.");
        }
    }

    public ArrayList<Float> getFeaturesValue() {
        return FeaturesValue;
    }

    public static ArrayList<String> getFeatureNameList() {
        ArrayList<String> FeatureNameList = new ArrayList<>();

        FeatureNameList.add("0_Mean"); FeatureNameList.add("1_Mean"); FeatureNameList.add("2_Mean");
        FeatureNameList.add("0_MAD"); FeatureNameList.add("1_MAD"); FeatureNameList.add("2_MAD");
        FeatureNameList.add("0_Var"); FeatureNameList.add("1_Var"); FeatureNameList.add("2_Var");
        FeatureNameList.add("0_sDev"); FeatureNameList.add("1_sDev"); FeatureNameList.add("2_sDev");
        FeatureNameList.add("0_Skew"); FeatureNameList.add("1_Skew"); FeatureNameList.add("2_Skew");
        FeatureNameList.add("0_Kurt"); FeatureNameList.add("1_Kurt"); FeatureNameList.add("2_Kurt");
        FeatureNameList.add("0_ZCR"); FeatureNameList.add("1_ZCR"); FeatureNameList.add("2_ZCR");
        FeatureNameList.add("0_MCR"); FeatureNameList.add("1_MCR"); FeatureNameList.add("2_MCR");
        FeatureNameList.add("0_AAD"); FeatureNameList.add("1_AAD"); FeatureNameList.add("2_AAD");
        FeatureNameList.add("0_AAV"); FeatureNameList.add("1_AAV"); FeatureNameList.add("2_AAV");
        FeatureNameList.add("0_RMS"); FeatureNameList.add("1_RMS"); FeatureNameList.add("2_RMS");
        FeatureNameList.add("0_Med"); FeatureNameList.add("1_Med"); FeatureNameList.add("2_Med");
        FeatureNameList.add("0_Corr"); FeatureNameList.add("1_Corr"); FeatureNameList.add("2_Corr");
        FeatureNameList.add("0_Cov"); FeatureNameList.add("1_Cov"); FeatureNameList.add("2_Cov");
        FeatureNameList.add("ARA");
        FeatureNameList.add("VarOfMag");

        return FeatureNameList;
    }

    // --------------------------------------------------------------------------- CALLER
    public FeatureExtract(){
        super();
    }

    public FeatureExtract(float[] C1, float[] C2, float[] C3){

        //For testing feature extraction
//        C1.add(1.5);
//        C1.add(2.5);
//        C1.add(3.5);
//        C1.add(1.5);
//
//        C2.add(1.5);
//        C2.add(2.5);
//        C2.add(1.5);
//        C2.add(3.5);
//
//        C3.add(3.5);
//        C3.add(1.5);
//        C3.add(1.5);
//        C3.add(2.5);

        setE1(C1);
        setE2(C2);
        setE3(C3);

        compute();
    }

    public void compute(){
        getFeaturesValue().add((float) getE1().getMean());
        getFeaturesValue().add((float) getE2().getMean());
        getFeaturesValue().add((float) getE3().getMean());

        getFeaturesValue().add((float) getE1().getMeanAbsDev());
        getFeaturesValue().add((float) getE2().getMeanAbsDev());
        getFeaturesValue().add((float) getE3().getMeanAbsDev());

        getFeaturesValue().add((float) getE1().getVar());
        getFeaturesValue().add((float) getE2().getVar());
        getFeaturesValue().add((float) getE3().getVar());

        getFeaturesValue().add((float) getE1().getStandardDeviation());
        getFeaturesValue().add((float) getE2().getStandardDeviation());
        getFeaturesValue().add((float) getE3().getStandardDeviation());

        getFeaturesValue().add((float) getE1().getSkew());
        getFeaturesValue().add((float) getE2().getSkew());
        getFeaturesValue().add((float) getE3().getSkew());

        getFeaturesValue().add((float) getE1().getKurt());
        getFeaturesValue().add((float) getE2().getKurt());
        getFeaturesValue().add((float) getE3().getKurt());

        getFeaturesValue().add((float) getE1().getZCR());
        getFeaturesValue().add((float) getE2().getZCR());
        getFeaturesValue().add((float) getE3().getZCR());

        getFeaturesValue().add((float) getE1().getMCR());
        getFeaturesValue().add((float) getE2().getMCR());
        getFeaturesValue().add((float) getE3().getMCR());

        getFeaturesValue().add((float) getE1().getAAD());
        getFeaturesValue().add((float) getE2().getAAD());
        getFeaturesValue().add((float) getE3().getAAD());

        getFeaturesValue().add((float) getE1().getAAV());
        getFeaturesValue().add((float) getE2().getAAV());
        getFeaturesValue().add((float) getE3().getAAV());

        getFeaturesValue().add((float) getE1().getRMS());
        getFeaturesValue().add((float) getE2().getRMS());
        getFeaturesValue().add((float) getE3().getRMS());

        getFeaturesValue().add((float) getE1().getMedian());
        getFeaturesValue().add((float) getE2().getMedian());
        getFeaturesValue().add((float) getE3().getMedian());

        getFeaturesValue().add((float) getCorrelation(getE1(), getE2()));
        getFeaturesValue().add((float) getCorrelation(getE2(), getE3()));
        getFeaturesValue().add((float) getCorrelation(getE1(), getE3()));

        getFeaturesValue().add((float) getCov(getE1(), getE2()));
        getFeaturesValue().add((float) getCov(getE2(), getE3()));
        getFeaturesValue().add((float) getCov(getE1(), getE3()));

        getFeaturesValue().add((float) getARA(getE1(), getE2(), getE3()));
        getFeaturesValue().add((float) getVarOfMag(getE1(), getE2(), getE3()));

        //System.out.println(FeaturesValue.toString());
    }

    // --------------------------------------------------------------------------- FEATURE CALCULATIONS

    private static class Element {

        ArrayList<Float> dataset = null;

        Element(ArrayList<Float> dataset) {
            this.dataset = dataset;
        }

        double getMean() {
            Mean mean = new Mean();
            for (Float aDataset : dataset) {
                mean.increment(aDataset);
            }
            return mean.getResult();
        }

        private double getMeanAbsDev() {
            double mad = 0;
            double mean = getMean();

            for (Float aDataset : dataset) {
                mad += Math.abs(aDataset - mean);
            }

            return mad/dataset.size();
        }

        double getVar() {
            Variance var = new Variance();
            for (Float aDataset : dataset) {
                var.increment(aDataset);
            }
            return var.getResult();
        }

        double getStandardDeviation() {
            double variance = getVar();
            return Math.sqrt(variance);
        }

        double getSkew() {
            Skewness skew = new Skewness();
            for (Float aDataset : dataset) {
                skew.increment(aDataset);
            }
            return skew.getResult();
        }

        double getKurt() {
            Kurtosis kurt = new Kurtosis();
            for (Float aDataset : dataset) {
                kurt.increment(aDataset);
            }
            return kurt.getResult();
        }

        int getZCR() {
            int count = 0;
            for (int i = 0; i < dataset.size() - 1; i++) {
                if (dataset.get(i) * dataset.get(i + 1) < 0) {
                    count++;
                }
            }
            return count;
        }

        int getMCR() {
            double mean = getMean();
            int count = 0;
            for (int i = 0; i < dataset.size() - 1; i++) {
                if ((dataset.get(i) > mean && dataset.get(i + 1) < mean)
                        || (dataset.get(i) < mean && dataset.get(i + 1) > mean)) {
                    count++;
                }
            }
            return count;
        }

        double getAAD() {
            double dif = 0;
            Abs abs = new Abs();
            for (int i = 0; i < dataset.size() - 1; i++) {
                dif += abs.value(dataset.get(i + 1) - dataset.get(i));
            }
            return dif;
        }

        double getAAV() {
            double sum = 0;
            Abs abs = new Abs();
            for (Float aDataset : dataset) {
                sum += abs.value(aDataset);
            }
            return sum / dataset.size();
        }

        double getRMS() {
            double sum = 0;
            Pow pow = new Pow();
            for (Float aDataset : dataset) {
                sum += pow.value(aDataset, 2);
            }
            return pow.value(sum / dataset.size(), 0.5);
        }

        double getMedian(){
            if(dataset.size() % 2 == 0){
                // even
                int valuePos = dataset.size() / 2;
                return (dataset.get(valuePos) + dataset.get(valuePos+1))/2;
            } else {
                // odd
                return dataset.get(dataset.size() / 2);
            }

        }

        public ArrayList<Double> fft_magnitude(boolean DIRECT) {
            // - n is the dimension of the problem
            // - nu is its logarithm in base e
            int n = dataset.size();
            double[] inputImag = new double[n];

            // If n is a power of 2, then ld is an integer (_without_ decimals)
            double ld = Math.log(n) / Math.log(2.0);

            // Here I check if n is a power of 2. If exist decimals in ld, I
            // quit
            // from the function returning null.
            if (((int) ld) - ld != 0) {
                System.out.println("The number of elements is not a power of 2.");
                return null;
            }

            // Declaration and initialization of the variables
            // ld should be an integer, actually, so I don't lose any
            // information in
            // the cast
            int nu = (int) ld;
            int n2 = n / 2;
            int nu1 = nu - 1;
            double[] xReal = new double[n];
            double[] xImag = new double[n];
            double tReal, tImag, p, arg, c, s;

            // Here I check if I'm going to do the direct transform or the
            // inverse
            // transform.
            double constant;
            if (DIRECT)
                constant = -2 * Math.PI;
            else
                constant = 2 * Math.PI;

            // I don't want to overwrite the input arrays, so here I copy them.
            // This
            // choice adds \Theta(2n) to the complexity.
            for (int i = 0; i < n; i++) {
                xReal[i] = dataset.get(i);
                xImag[i] = inputImag[i];
            }

            // First phase - calculation
            int k = 0;
            for (int l = 1; l <= nu; l++) {
                while (k < n) {
                    for (int i = 1; i <= n2; i++) {
                        p = bitreverseReference(k >> nu1, nu);
                        // direct FFT or inverse FFT
                        arg = constant * p / n;
                        c = Math.cos(arg);
                        s = Math.sin(arg);
                        tReal = xReal[k + n2] * c + xImag[k + n2] * s;
                        tImag = xImag[k + n2] * c - xReal[k + n2] * s;
                        xReal[k + n2] = xReal[k] - tReal;
                        xImag[k + n2] = xImag[k] - tImag;
                        xReal[k] += tReal;
                        xImag[k] += tImag;
                        k++;
                    }
                    k += n2;
                }
                k = 0;
                nu1--;
                n2 /= 2;
            }

            // Second phase - recombination
            k = 0;
            int r;
            while (k < n) {
                r = bitreverseReference(k, nu);
                if (r > k) {
                    tReal = xReal[k];
                    tImag = xImag[k];
                    xReal[k] = xReal[r];
                    xImag[k] = xImag[r];
                    xReal[r] = tReal;
                    xImag[r] = tImag;
                }
                k++;
            }

            // Here I have to mix xReal and xImag to have an array (yes, it
            // should
            // be possible to do this stuff in the earlier parts of the code,
            // but
            // it's here to readibility).
            ArrayList<Double> newArray = new ArrayList<>(xReal.length);
            //float[] newArray = new float[xReal.length];
            double radice = 1 / Math.sqrt(n);
            for (int i = 0; i < xReal.length; i++) {

                // I Modified Original Program so that it will only produce fft
                // magnitude array.
                newArray.add(Math.pow((Math.pow(xReal[i] * radice, 2) + Math.pow(xImag[i] * radice, 2)), 0.5));
            }
            return newArray;
        }

        private static int bitreverseReference(int j, int nu) {
            int j2;
            int j1 = j;
            int k = 0;
            for (int i = 1; i <= nu; i++) {
                j2 = j1 / 2;
                k = 2 * k + j1 - 2 * j2;
                j1 = j2;
            }
            return k;
        }
    }

    private static double getCorrelation(Element e1, Element e2){

        double cov = getCov(e1,e2);
        double standardDeviation1 = e1.getStandardDeviation();
        double standardDeviation2 = e2.getStandardDeviation();

        if(e1.dataset.size()!=e2.dataset.size()){
            throw new IllegalArgumentException("Two input matrices should have same dimension");
        }

        return cov / (standardDeviation1 * standardDeviation2);
    }

    private static double getCov(Element e1, Element e2) {
        double sum = 0;
        for (int i = 0; i < e1.dataset.size(); i++) {
            sum += (e1.dataset.get(i) - e1.getMean()) * (e2.dataset.get(i) - e2.getMean());
        }
        return sum / (e1.dataset.size() - 1);
    }

    private static double getARA(Element e1, Element e2, Element e3) {
        double sum = 0, tmp;
        Pow pow = new Pow();
        for (int i = 0; i < e1.dataset.size(); i++) {
            tmp = pow.value(e1.dataset.get(i), 2) + pow.value(e2.dataset.get(i), 2) + pow.value(e3.dataset.get(i), 2);
            sum += pow.value(tmp, 0.5);
        }
        return sum / e1.dataset.size();
    }

    private static double getMag(Element e1, Element e2, Element e3, int idx) {
        // return one value from samples
        Pow pow = new Pow();
        if (idx > e1.dataset.size()) {
            return -1;
        }
        idx -= 1;

        double sum = pow.value(e1.dataset.get(idx), 2) + pow.value(e2.dataset.get(idx), 2) + pow.value(e3.dataset.get(idx), 2);
        return pow.value(sum, 0.5);
    }

    private static double getMag(double e1, double e2, double e3) {
        // return one value from samples
        Pow pow = new Pow();
        double sum = pow.value(e1, 2) + pow.value(e2, 2) + pow.value(e3, 2);
        return pow.value(sum, 0.5);
    }

    private static double getVarOfMag(Element e1, Element e2, Element e3){
        Variance var = new Variance();
        for (int i = 0; i < e1.dataset.size(); i++) {
            double mag = getMag(e1.dataset.get(i), e2.dataset.get(i), e3.dataset.get(i));
            var.increment(mag);
        }
        return var.getResult();
    }

}
