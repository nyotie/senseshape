package Utils;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;

/**
 * Created by nyotie on 11-Jun-17.
 */
public class FileWorker {

    public static void RenameFile(File original, File endProduct){
        try{
            Files.delete(original.toPath());
            endProduct.renameTo(original);
        }catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n", original.toPath());
        } catch (DirectoryNotEmptyException x) {
            System.err.format("%s not empty%n", original.toPath());
        } catch (IOException x) {
            // File permission problems are caught here.
            System.err.println(x);
        }
    }

    public static void DeleteFile(File deleteThis){
        try{
            Files.delete(deleteThis.toPath());
        }catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n", deleteThis.toPath());
        } catch (DirectoryNotEmptyException x) {
            System.err.format("%s not empty%n", deleteThis.toPath());
        } catch (IOException x) {
            // File permission problems are caught here.
            System.err.println(x);
        }
    }

    /**
     * Merge two files by appending each second data's line to the end of each line in first data
     *
     * @param pathToA Path to the first file
     * @param pathToB Path to the second file
     * @param outputFile Path to the output file
     */
    public static void MergeFileHorizontally(String pathToA, String pathToB, String outputFile, Boolean deleteA, boolean deleteB) throws IOException {
        BufferedReader buffA = new BufferedReader(new FileReader(pathToA));
        BufferedReader buffB = new BufferedReader(new FileReader(pathToB));

        BufferedWriter out = new BufferedWriter(new FileWriter(outputFile), 32768);

        while (true) {
            String partOne = buffA.readLine();
            String partTwo = buffB.readLine();

            if (partOne == null || partTwo == null)
                break;

            out.write(partOne + "," + partTwo);
            out.newLine();
        }
        buffA.close();
        buffB.close();
        out.close();

        if(deleteA) DeleteFile(new File(pathToA));
        if(deleteB) DeleteFile(new File(pathToB));
    }

    /**
     * Merge two files by appending each second data's line to the end of each line in first data
     *
     * @param pathToFiles Path to the files
     * @param outputPath Path to the output file
     */
    public static void MergeFileHorizontally(ArrayList<String> pathToFiles, String outputPath, Boolean deleteSource) throws IOException {

        ArrayList<BufferedReader> sumber = new ArrayList<>();
        for(String path : pathToFiles){
            BufferedReader fileX = new BufferedReader(new FileReader(path));
            sumber.add(fileX);
        }

        BufferedWriter out = new BufferedWriter(new FileWriter(outputPath), 32768);

        boolean endOfLine = false;
        while (!endOfLine) {
            StringBuilder txt = new StringBuilder();

            int counter=0;
            for(BufferedReader fileX: sumber){
                String line = fileX.readLine();
                if (line == null) {
                     endOfLine=true;
                     break;
                }

                txt.append(line);
                if(counter != sumber.size()-1) txt.append(",");
                counter++;
            }

            out.write(txt.toString());
            out.newLine();
        }

        if(deleteSource){
            for(String path : pathToFiles){
                DeleteFile(new File(path));
            }
        }

        out.close();
    }

    /**
     * Merge files by stacking second data after the last line of first data
     *
     * @param PathIn the list of the files to be merged
     * @param PathOut Path of the output file
     * @throws IOException
     */
    public static void MergeFileVertically(ArrayList<String> PathIn, String PathOut) throws IOException {

        //Get channel for output file
        FileOutputStream fos = new FileOutputStream(new File(PathOut));
        WritableByteChannel targetChannel = fos.getChannel();

        for (String file: PathIn){
            //Get channel for input files
            FileInputStream fis = new FileInputStream(file);
            FileChannel inputChannel = fis.getChannel();

            //Transfer data from input channel to output channel
            inputChannel.transferTo(0, inputChannel.size(), targetChannel);

            //close the input channel
            inputChannel.close();
            fis.close();
        }

        //finally close the target channel
        targetChannel.close();
        fos.close();
    }

    public static void main(String[] args) throws IOException {

    }
}
