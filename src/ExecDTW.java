import com.opencsv.CSVReader;
import distance.DTW;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ExecDTW {
    public static void main(String[] args) throws Exception {
        String dirFileSamples= "D:/experiment/distance.DTW files/template";
        String dirFileTemplates = "D:/experiment/distance.DTW files/search_field";

        // create path bank of template from dir
        ArrayList<String> dirSampleList = new ArrayList<>();
        try(Stream<Path> paths = Files.walk(Paths.get(dirFileSamples)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> dirSampleList.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // create path bank of samples from dir
        ArrayList<String> dirTemplateList = new ArrayList<>();
        try(Stream<Path> paths = Files.walk(Paths.get(dirFileTemplates)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> dirTemplateList.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
//
//        CSVLoader cnv = new CSVLoader();
//        cnv.setSource(new File(bestFilePath));
//        Instances bestSample = cnv.getDataSet();
//        if (bestSample.classIndex() == -1)
//            bestSample.setClassIndex(bestSample.numAttributes() - 1);
//
//        System.out.println(bestSample.instance(0));
//
//        IBk knn_DTW = new IBk();
//        knn_DTW.setKNN(1);
//        NearestNeighbourSearch search = new LinearNNSearch();
//        DistanceFunction dtw = new distance.DTW();
//        search.setDistanceFunction(dtw);
//        knn_DTW.setNearestNeighbourSearchAlgorithm(search);
//        // as in tutorial : https://github.com/mkannwischer/kNN/blob/master/src/tud/ke/ml/project/junit/AdvancedValidation.java
//

        for (String sampleFile: dirSampleList){
            // read tempate csv and store to variable
            CSVReader csvFromVictim = new CSVReader(new FileReader(sampleFile));
            int victimColCount = csvFromVictim.readNext().length - 1;
            List<String[]> victimValues = csvFromVictim.readAll();
            ArrayList<float[]> theVictim = rowToColumn(victimValues, victimColCount);

//        example to read all rows and print each column
//        List<String[]> myEntries = reader.readAll();
//        for (String[] lineTokens : myEntries) {
//            for (String token : lineTokens) {
//                System.out.println(token);
//            }
//        }
//        ------------------
//        another example
//        String[] nextLineFromSample;
//        while ((nextLineFromSample = reader.readNext()) != null) {
//            System.out.println(nextLineFromSample[0]);
//        }
//        ------------------
//        another example
//        ArrayList<String[]> outerArr = new ArrayList<String[]>();
//        String[] myString1= {"hey","not","yes","oh"};
//        outerArr .add(myString1);
//        String[] myString2= {"you","her","him","God"};
//        outerArr .add(myString2);
//
//        //retrieve using:
//        for(int i=0;i<outerArr.size();i++){
//
//            String[] myString;
//            myString=outerArr.get(i);
//            for(int j=0;j<myString.length;j++){
//                System.out.print(myString[j]);
//            }
//            System.out.print("\n");
//
//        }

            double[] distanceCollection = new double[dirTemplateList.size()];
            int fileCount = 0;
            int iLowestDistance =0;
            double lastLowest = 0;

            //start iterate csv files in path bank
            for (String filePathName: dirTemplateList){
                CSVReader csvFromBank = new CSVReader(new FileReader(filePathName));
                int suspectColCount = csvFromBank.readNext().length - 1;
                List<String[]> suspectValues = csvFromBank.readAll();
                ArrayList<float[]> theSuspect = rowToColumn(suspectValues, suspectColCount);

                double[] distResult = new double[suspectColCount];

                //todo: calculate distance of each features of each axes using distance.DTW between victim and suspect
                // assume column count is the same
                for (int i = 0; i <= victimColCount -1; ++i){
                    //calculate the distance
                    DTW calcDTW = new DTW(theVictim.get(i), theSuspect.get(i));

                    //store distance result
                    distResult[i] = calcDTW.getWarpingDistance();
                }

                //todo: average the accumulated distance result
                double sumOfDistance = 0;
                for(double mark: distResult)
                    sumOfDistance+=mark;
                distanceCollection[fileCount] = sumOfDistance / distResult.length;

                //todo: compare all calculated distance and choose the minimum one
                if (lastLowest == 0 || distanceCollection[fileCount] < lastLowest) {
                    iLowestDistance = fileCount;
                    lastLowest = distanceCollection[fileCount];
                }
//            System.out.println("Comparing with " + dirList.get(fileCount) + ", avg distance: " + avg);
                fileCount++;
            }

            System.out.println("Comparing " + sampleFile + " with " + dirTemplateList.get(iLowestDistance) + ". fileNumber:" + iLowestDistance + ", distance:" + distanceCollection[iLowestDistance]);
        }
    }

    /**
     *
     * @param thisIsShit array list to convert
     * @param colCount column count
     * @return
     */
    private static ArrayList<float[]> rowToColumn(List<String[]> thisIsShit, int colCount){
        ArrayList<float[]> notShit = new ArrayList<>();

        for(int i = 0; i <= colCount-1; ++i){ //iterate column, not including the last column (is a string for label)
            float[] tempDoubleArrayList = new float[thisIsShit.size()];
            int rowCount = 0;

            for (String[] lineTokens : thisIsShit) {
                tempDoubleArrayList[rowCount] = Float.valueOf(lineTokens[i]);
                rowCount++;
            }
            notShit.add(tempDoubleArrayList);
        }

        return notShit;
    }
}
